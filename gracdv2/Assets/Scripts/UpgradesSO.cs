using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Upgrades", menuName = "ScriptableObject/UpgradesAsset")]

public class UpgradesSO : ScriptableObject
{
    // LIFE
    public bool shieldUpgrade;

    public bool savingShieldUpgrade;
    public float shieldCooldown;

    public bool extraToughUpgrade;

    // STRENGTH
    public bool fasterShootUpgrade;
    public float fasterShootSpeed;

    public bool powerUPUpgrade;

    public bool bulletsUpgrade;

    // MISC
    public bool boosterUpgrade;
    public float boosterAdditionalTime;

    public bool maskingUpgrade;
    public float maskingTime;
    public float maskingCooldown;

    public bool bombUpgrade;
    public float bombTime;
    public float bombCooldown;
    public float bombRadius;
}
