using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SicknessStation : MonoBehaviour
{
    public float maxSickness = 1f;
    private float currentSickness = 0;
    public float sicknessRaiseSpeed = 1f;
    public bool isSick { get; private set; }

    public float sicknessLevelRaise = 5f;

    public Image bar;

    public GameObject station;
    public GameObject stationInfected;
    public GameObject ui;
    [SerializeField] private ParticleSystem ps;
    //private AudioManager am;
    private AudioSource ass;
    private bool isPlaying;
    private void Start()
    {
        isSick= false;
        station.SetActive(true);
        stationInfected.SetActive(false);
        ui.SetActive(true);
        //am = FindObjectOfType<AudioManager>();
        ass = GetComponent<AudioSource>();
        GetCurrentFill();
        isPlaying = false;
    }

    private void Update()
    {
        if(currentSickness >= maxSickness && !isSick)
        {
            //am.PlaySound("infectingStation");
            // raise SicknessLevel
            SicknessManager.Instance.currentSicknessLvl += sicknessLevelRaise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.infectedStations++;

            isSick= true;
            //this.gameObject.SetActive(false); // change texture
            stationInfected.SetActive(true);
            station.SetActive(false);
            ui.SetActive(false);
            ps.Play();
        }
        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            ass.Stop();
            isPlaying = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            //ass.Play();
        }
        if (Input.GetKey(KeyCode.LeftShift) && other.CompareTag("Player") && !isSick && !UIManager.Instance.isPaused)
        {
            //ass.Play();
            //am.PlaySound("infectingStation");
            RaiseSickness();
            GetCurrentFill();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        ass.Stop();
        isPlaying = false;
    }

    public void RaiseSickness()
    {
        currentSickness += sicknessRaiseSpeed * Time.deltaTime;
        if (!isPlaying)
        {
            ass.Play();
            isPlaying = true;
        }
    }

    private void GetCurrentFill()
    {
        float fill = currentSickness / maxSickness;
        bar.fillAmount = fill;
    }
}
