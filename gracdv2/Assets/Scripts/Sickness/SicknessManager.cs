using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class SicknessManager : MonoBehaviour
{
    public static SicknessManager Instance;

    public float maxSicknessLvl = 100f;
    public float minSicknessLvl = 0f;
    public float startSicknessLvl;
    public float currentSicknessLvl;
    public Difficulty currentDifficulty;
    public GameObject bossPrefab;
    public Vector3 bossSpawn;
    [HideInInspector] public int killedEnemies;
    [HideInInspector] public int infectedStations;
    [HideInInspector] public int damageReceived;
    [SerializeField] private GameObject changeDifficultyInfo;
    [SerializeField] private float infoTime;
    [HideInInspector] public bool spawnBoss;
    [HideInInspector] public bool restartPossible;
    public StatisticsGO restartStats;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        killedEnemies = 0;
        infectedStations = 0;
        damageReceived = 0;
        currentSicknessLvl = startSicknessLvl;
        //currentDifficulty = Difficulty.Easy;
        changeDifficultyInfo.SetActive(false);
        spawnBoss = false;
        restartPossible = false;
    }

    private void Start()
    {
        BossRestart();
        CheckDifficulty();
    }
    private void Update()
    {
        //Debug.Log(killedEnemies);
        currentSicknessLvl = Mathf.Round(currentSicknessLvl * 100f) / 100f;
        if(currentDifficulty == Difficulty.Boss && spawnBoss)
        {
            Instantiate(bossPrefab, bossSpawn, Quaternion.identity);
            spawnBoss = false;
        }
    }

    public void CheckDifficulty()
    {
        if(InGameUI.Instance != null) InGameUI.Instance.ThermometrPuls();
        if (currentSicknessLvl >= SectionTrigger.Instance.sicknessThresholdMedium && currentDifficulty == Difficulty.Easy)
        {
            StartCoroutine(ChangeDifficultyInfo());
            AudioManager.Instance.PlaySound("difficultyChange");
            currentDifficulty = Difficulty.Medium;
            SectionTrigger.Instance.lastSectionSpawnIndex = -1;
        }
        if (currentSicknessLvl >= SectionTrigger.Instance.sicknessThresholdHard && currentDifficulty == Difficulty.Medium)
        {
            StartCoroutine(ChangeDifficultyInfo());
            AudioManager.Instance.PlaySound("difficultyChange");
            currentDifficulty = Difficulty.Hard;
            SectionTrigger.Instance.lastSectionSpawnIndex = -1;
        }
        if (currentSicknessLvl >= SectionTrigger.Instance.sicknessThresholdBoss && currentDifficulty == Difficulty.Hard)
        {
            StartCoroutine(ChangeDifficultyInfo());
            AudioManager.Instance.PlaySound("difficultyChange");
            currentDifficulty = Difficulty.Boss;
            SectionTrigger.Instance.lastSectionSpawnIndex = -1;
        }
        if (currentSicknessLvl >= maxSicknessLvl) currentSicknessLvl = maxSicknessLvl;
    }

    IEnumerator ChangeDifficultyInfo()
    {
        changeDifficultyInfo.SetActive(true);
        yield return new WaitForSeconds(infoTime);
        changeDifficultyInfo.SetActive(false);
    }

    private void BossRestart()
    {
        if(restartStats.level1Points == -1)
        {
            killedEnemies = restartStats.enemiesKilled;
            damageReceived = restartStats.damageReceived;
            infectedStations = restartStats.stationsInfected;
            TimeManager.Instance.currentTime = restartStats.time;
            restartPossible = true;
            currentSicknessLvl = 100;
            currentDifficulty = Difficulty.Boss;
        }
    }
}

public enum Difficulty
{
    Easy = 1,
    Medium = 2,
    Hard = 3,
    Boss = 4
}
