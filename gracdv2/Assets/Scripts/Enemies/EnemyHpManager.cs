using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHpManager : MonoBehaviour
{
    public EnemiesShooting esConfig;
    public int currentHP;

    private void Awake()
    {
        currentHP = esConfig.hpMax;
    }

    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            currentHP -= PlayerManager.Instance.playerCurrentDmg;
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Explode"))
        {
            other.GetComponentInChildren<Bullet_Explode>().BulletExplode();
            currentHP -= PlayerManager.Instance.playerCurrentDmg;
        }
    }
}
