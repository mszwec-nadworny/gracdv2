using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class EnemyHealer : MonoBehaviour
{
    public EnemieNotShooting ensConfig;
    [SerializeField] private ParticleSystem psDeath;
    [SerializeField] private ParticleSystem psRipple;
    private EnemyHpManagerNotShooting eHpM;
    private bool waitForHeal;
    private Vector3 startPosition;
    public float healingRadius = 3.16f;
    private RaycastHit[] hits;
    [SerializeField] private GameObject parent;
    private void Start()
    {
        eHpM = GetComponent<EnemyHpManagerNotShooting>();
        waitForHeal = false;
    }
    private void Update()
    {
        if (!waitForHeal)
        {
            Heal();
        }
        if (UIManager.Instance.isBossHere)
        {
            Destroy(parent);
        }
        if (eHpM.currentHP <= 0)
        {
            psDeath.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += ensConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += ensConfig.points;
            if (!UIManager.Instance.isBossHere)
            {
                HealerManager.Instance.WaitToSpawnHealer();
            }
            Destroy(parent);
        }
    }

    private void Heal()
    {
        waitForHeal = true;

        startPosition = this.transform.position;

        hits = Physics.SphereCastAll(startPosition, healingRadius, transform.forward, 0);

        psRipple.Play();

        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Enemy") && hit.collider.GetComponent<EnemyHealer>() == null)
            {
                if (hit.collider.GetComponent<EnemyHpManager>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManager>().currentHP = hit.collider.GetComponent<EnemyHpManager>().esConfig.hpMax;
                }
                else if (hit.collider.GetComponent<EnemyHpManagerNotShooting>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManagerNotShooting>().currentHP = hit.collider.GetComponent<EnemyHpManagerNotShooting>().ensConfig.hpMax;
                }
            }
        }

        StartCoroutine(WaitToHeal());
    }

    IEnumerator WaitToHeal()
    {
        yield return new WaitForSeconds(ensConfig.betweenDamageTime);
        waitForHeal = false;
    }
}
