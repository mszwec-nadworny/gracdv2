using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDebuffer : MonoBehaviour
{
    public EnemieNotShooting ensConfig;

    // debuff on collider enter with time
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && ensConfig.speedDebuff != 0)
        {
            if (!PlayerManager.Instance.isDebuffed && !PlayerManager.Instance.isInWind)
            {
                PlayerManager.Instance.ChangeSpeed(ensConfig.speedDebuff, ensConfig.speedDebuffTime);
            }
        }
    }
}
