using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1Behaviour : MonoBehaviour
{
    public EnemiesShooting esConfig;
    public int currentHP;

    public Transform bulletSpawn1;
    public Transform bulletSpawn2;
    public Transform bulletSpawn3;
    public GameObject bulletPrefab;
    private bool canUShoot;

    private bool waitForDamage;

    public Image hpBar;

    [SerializeField] public ParticleSystem ps;
    private void Start()
    {
        currentHP = esConfig.hpMax;
        GetCurrentFill();
        canUShoot = true;
        UIManager.Instance.bossHP = currentHP;        
        UIManager.Instance.isBossHere = true;
        StopAllCoroutines();
    }
    void Update()
    {
        if (canUShoot)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Shoot();
            StartCoroutine(CantShoot());
        }

        // killed and raise sickness
        if (currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("bossDeath");
            SicknessManager.Instance.killedEnemies++;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // damage on touch

        if (other.CompareTag("Player") && !PlayerManager.Instance.takeNoDmg)
        {
            AudioManager.Instance.PlaySound("damagePlayer");
            PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
            PlayerManager.Instance.TakeNoDmgFunction();
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            AudioManager.Instance.PlaySound("damageEnemy");
            currentHP -= PlayerManager.Instance.playerCurrentDmg;
            GetCurrentFill();
            UIManager.Instance.bossHP = currentHP;
            Destroy(other.gameObject);
        }

    }

    // damage if you stay near

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !waitForDamage && !PlayerManager.Instance.takeNoDmg)
        {
            AudioManager.Instance.PlaySound("damagePlayer");
            PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
            PlayerManager.Instance.TakeNoDmgFunction();
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }

    IEnumerator WaitForDamage()
    {
        yield return new WaitForSeconds(esConfig.betweenDamageTime);
        waitForDamage = false;
    }

    private void Shoot()
    {
        GameObject cb1 = Instantiate(bulletPrefab, bulletSpawn1.position, Quaternion.identity) as GameObject;
        GameObject cb2 = Instantiate(bulletPrefab, bulletSpawn2.position, Quaternion.identity) as GameObject;
        GameObject cb3 = Instantiate(bulletPrefab, bulletSpawn3.position, Quaternion.identity) as GameObject;
        //Rigidbody rig = cb.GetComponent<Rigidbody>();

        //rig.AddForce(new Vector3(-1,0,0)*esConfig.bulletSpeed, ForceMode.Impulse);
    }
    IEnumerator CantShoot()
    {
        canUShoot = false;
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }

    private void GetCurrentFill()
    {
        float fill = (float)currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }
}
