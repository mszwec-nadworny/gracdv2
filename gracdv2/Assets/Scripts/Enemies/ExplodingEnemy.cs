using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class ExplodingEnemy : MonoBehaviour
{
    public EnemieNotShooting ensConfig;
    public int currentHP;
    private bool waitForDamage;
    [SerializeField] private ParticleSystem ps;
    [SerializeField] private ParticleSystem explosionParticle;
    public GameObject explosionField;
    public float explosionTime;
    private bool canUMove;

    [SerializeField] private Animator anim;
    [SerializeField] private Material mymat;
    [SerializeField] private GameObject parent;
    private void Start()
    {
        currentHP = ensConfig.hpMax;
        waitForDamage = false;
        canUMove= false;
        explosionField.SetActive(false);
        anim.SetBool("Explode", false);
        mymat.DisableKeyword("_EMISSION");
        DynamicGI.UpdateEnvironment();
    }

    // movement
    void Update()
    {
        if(canUMove)
        {
            parent.transform.Translate(Vector3.left * Time.deltaTime * ensConfig.moveSpeed, Space.World);

        }

        // killed and raise sickness
        if (currentHP <= 0 && canUMove)
        {
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += ensConfig.points;
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        // start shooting
        if (other.CompareTag("Right_Border"))
        {
            canUMove = true;
        }
    }

    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        // damage on touch

        if (other.CompareTag("Player") && ensConfig.damageOnTouch != 0 && !waitForDamage)
        {
            currentHP = 0;
        }

        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            if (!waitForDamage) 
            { 
                currentHP -= PlayerManager.Instance.playerCurrentDmg; 
            }
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Explode"))
        {
            other.GetComponentInChildren<Bullet_Explode>().BulletExplode();
            if (!waitForDamage)
            {
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
            }
            Destroy(other.gameObject);
        }
    }

    IEnumerator WaitForDamage()
    {
        canUMove = false;
        //Material mymat = GetComponent<Renderer>().material;
        mymat.EnableKeyword("_EMISSION");
        //mymat.SetColor("_EmissionColor", Color.red);
        DynamicGI.UpdateEnvironment();
        //DynamicGI.SetEmissive(this.GetComponentInChildren<Renderer>, Color.red);
        anim.SetBool("Explode", true);

        yield return new WaitForSeconds(ensConfig.betweenDamageTime);
        mymat.DisableKeyword("_EMISSION");
        DynamicGI.UpdateEnvironment();

        anim.SetBool("Explode", false);

        explosionField.SetActive(true);
        explosionParticle.Play();
        AudioManager.Instance.PlaySound("explosion");
        yield return new WaitForSeconds(explosionTime);
        ps.Play();
        this.gameObject.SetActive(false);
    }
}
