using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Boss4 : MonoBehaviour
{
    public EnemiesShooting esConfig;
    private int currentHP;
    private bool canUShoot;
    [SerializeField] private List<Transform> bulletSpawns;
    [SerializeField] private GameObject bullet;
    [SerializeField] private ParticleSystem psDeath;
    [SerializeField] private Image hpBar;
    [SerializeField] private TextMeshProUGUI stationsTxt;
    [SerializeField] private GameObject shield;
    [SerializeField] private float shieldOffTime;
    private bool isShieldOn;
    public int neededStations;
    [HideInInspector] public int currentStations;

    public static Boss4 Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        currentHP = esConfig.hpMax;
        canUShoot = true;
        UIManager.Instance.bossHP = currentHP;
        UIManager.Instance.isBossHere = true;
        isShieldOn = true;
        shield.SetActive(true);
        currentStations = 0;
        stationsTxt.text = (neededStations-currentStations).ToString();
        GetCurrentFill();
    }
    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            StartCoroutine(Shooting());
        }
        //  take off shield
        if (currentStations == neededStations && isShieldOn)
        {
            StartCoroutine(ShieldOff());
        }

        GetCurrentFill();

        // killed and raise sickness
        if (currentHP <= 0)
        {
            psDeath.Play();
            AudioManager.Instance.PlaySound("boss4");
            SicknessManager.Instance.killedEnemies++;
            UIManager.Instance.bossHP = currentHP;
            this.gameObject.SetActive(false);
        }
    }
    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            if (!isShieldOn)
            {
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
            }
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Explode"))
        {
            if (!isShieldOn)
            {
                other.GetComponentInChildren<Bullet_Explode>().BulletExplode();
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }    
    private void Shoot()
    {
        foreach (Transform bulletSpawn in bulletSpawns)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
        }
    }
    IEnumerator Shooting()
    {
        canUShoot = false;
        Shoot();
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }

    IEnumerator ShieldOff()
    {
        shield.SetActive(false);
        isShieldOn = false;
        yield return new WaitForSeconds(shieldOffTime);
        shield.SetActive(true);
        isShieldOn = true;
        currentStations = 0;
        stationsTxt.text = (neededStations - currentStations).ToString();
    }
    private void GetCurrentFill()
    {
        float fill = (float)currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }

    public void StationsNumber()
    {
        if(currentStations < neededStations)
        {
            currentStations++;
            stationsTxt.text = (neededStations - currentStations).ToString();
        }
    }
}
