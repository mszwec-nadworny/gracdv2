using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.UI;

public class Boss3 : MonoBehaviour
{
    public EnemiesShooting esConfig;
    public float shoot2wave;
    public int weakPointHP;
    public float stunTime;
    public float waitForIdle;
    private int currentHP;
    private bool canUShoot;
    [SerializeField] private List<Transform> bulletSpawns;
    [SerializeField] private List<Transform> bulletSpawns2;
    [SerializeField] private GameObject bullet;
    [SerializeField] private ParticleSystem psDeath;
    [SerializeField] private Image hpBar;
    //[SerializeField] private SectionSO sectionSpeed;
    //private float tempSectionSpeed;
    private bool isDamagable;
    [HideInInspector] public int destroyedWeakPoints;
    [SerializeField] private GameObject bossIn, bossRolled;
    [SerializeField] private GameObject weakPoint1, weakPoint2, weakPoint3;
    void Awake()
    {
        currentHP = esConfig.hpMax;
        canUShoot = false;
        isDamagable = false;
        destroyedWeakPoints = 0;
        //tempSectionSpeed = sectionSpeed.sectionSpeed;
        UIManager.Instance.bossHP = currentHP;
        UIManager.Instance.isBossHere = true;
        bossIn.SetActive(true);
        bossRolled.SetActive(false);
        GetCurrentFill();
        StartCoroutine(StartIdle());
    }

    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn && !isDamagable)
        {
            StartCoroutine(Shooting());
        }

        GetCurrentFill();

        // killed and raise sickness
        if (currentHP <= 0)
        {
            //sectionSpeed.sectionSpeed = tempSectionSpeed;
            psDeath.Play();
            AudioManager.Instance.PlaySound("boss3");
            SicknessManager.Instance.killedEnemies++;
            UIManager.Instance.bossHP = currentHP;
            this.gameObject.SetActive(false);
        }

        if(destroyedWeakPoints == 3)
        {
            StartCoroutine(Stun());
        }
    }

    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            if (isDamagable)
            {
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
            }
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Explode"))
        {
            if (isDamagable)
            {
                other.GetComponentInChildren<Bullet_Explode>().BulletExplode();
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }
    private void Shoot(List<Transform> bullets)
    {
        foreach (Transform bulletSpawn in bullets)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
        }
    }
    IEnumerator Shooting()
    {
        canUShoot = false;
        Shoot(bulletSpawns);
        yield return new WaitForSeconds(shoot2wave);
        Shoot(bulletSpawns2);
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }

    IEnumerator Stun()
    {
        //sectionSpeed.sectionSpeed = 0;
        destroyedWeakPoints = 0;
        isDamagable = true;
        yield return new WaitForSeconds(stunTime);
        isDamagable = false;
        //sectionSpeed.sectionSpeed = tempSectionSpeed;
        weakPoint1.SetActive(true);
        weakPoint1.GetComponent<Boss3WeakPoint>().ResetWeakPoint();
        weakPoint2.SetActive(true);
        weakPoint2.GetComponent<Boss3WeakPoint>().ResetWeakPoint();
        weakPoint3.SetActive(true);
        weakPoint3.GetComponent<Boss3WeakPoint>().ResetWeakPoint();
    }
    IEnumerator StartIdle()
    {
        yield return new WaitForSeconds(waitForIdle);
        bossIn.SetActive(false);
        bossRolled.SetActive(true);
        canUShoot = true;
        //this.GetComponent<Animator>().SetBool("startIdle", true);
        //Debug.Log("puff");
    }
    private void GetCurrentFill()
    {
        float fill = (float)currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }
}
