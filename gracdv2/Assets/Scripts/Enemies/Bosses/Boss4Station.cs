using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss4Station : MonoBehaviour
{
    [SerializeField] private float maxSickness = 1f;
    private float currentSickness = 0;
    [SerializeField] private float sicknessRaiseSpeed = 1f;
    private bool isSick;

    [SerializeField] private Image bar;
    [SerializeField] private GameObject station;
    [SerializeField] private GameObject ui;
    [SerializeField] private ParticleSystem ps;
    private AudioSource ass;
    private bool isPlaying;
    private void Start()
    {
        isSick = false;
        station.SetActive(true);
        ui.SetActive(true);
        ass = GetComponent<AudioSource>();
        GetCurrentFill();
        isPlaying = false;
    }

    private void Update()
    {
        if (currentSickness >= maxSickness && !isSick)
        {
            if (BossAnimationManager.Instance.startBoss)
            { // boss shield stations rise
                Boss4.Instance.StationsNumber();
            }

            isSick = true;
            station.SetActive(false);
            ui.SetActive(false);
            ps.Play();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            ass.Stop();
            isPlaying = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.LeftShift) && other.CompareTag("Player") && !isSick && !UIManager.Instance.isPaused)
        {
            RaiseSickness();
            GetCurrentFill();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        ass.Stop();
        isPlaying = false;
    }

    public void RaiseSickness()
    {
        currentSickness += sicknessRaiseSpeed * Time.deltaTime;
        if (!isPlaying)
        {
            ass.Play();
            isPlaying = true;
        }
    }

    private void GetCurrentFill()
    {
        float fill = currentSickness / maxSickness;
        bar.fillAmount = fill;
    }
}
