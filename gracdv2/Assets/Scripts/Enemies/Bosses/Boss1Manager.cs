using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Manager : MonoBehaviour
{
    [HideInInspector] public bool isMiniBoss1Dead;
    [HideInInspector] public bool isMiniBoss2Dead;
    [SerializeField] private GameObject bossAnimation;
    [SerializeField] private GameObject boss;
    [SerializeField] private float animationTime = 4f;
    private void Awake()
    {
        isMiniBoss1Dead = false;
        isMiniBoss2Dead = false;
        bossAnimation.SetActive(true);
        boss.SetActive(false);
        StartCoroutine(BossAnimating());
    }
    private void Update()
    {
        if(isMiniBoss1Dead && isMiniBoss2Dead)
        {
            UIManager.Instance.bossHP = 0;
        }
    }

    IEnumerator BossAnimating()
    {
        yield return new WaitForSeconds(animationTime);
        bossAnimation.SetActive(false);
        boss.SetActive(true);
    }
}
