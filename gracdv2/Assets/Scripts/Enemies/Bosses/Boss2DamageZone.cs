using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class Boss2DamageZone : MonoBehaviour
{
    [SerializeField] private EnemiesShooting esConfig;
    private bool waitForDamage;
    private void Awake()
    {
        waitForDamage = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !PlayerManager.Instance.takeNoDmg)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
                SicknessManager.Instance.damageReceived++;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !waitForDamage && !PlayerManager.Instance.takeNoDmg)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
                SicknessManager.Instance.damageReceived++;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            waitForDamage = false;
            StopAllCoroutines();
        }
    }
    IEnumerator WaitForDamage()
    {
        yield return new WaitForSeconds(esConfig.betweenDamageTime);
        waitForDamage = false;
    }
}
