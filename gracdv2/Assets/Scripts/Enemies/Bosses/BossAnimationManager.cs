using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationManager : MonoBehaviour
{
    [SerializeField] private GameObject bossAnimation;
    [SerializeField] private GameObject boss;
    [SerializeField] private float animationTime;
    [HideInInspector] public bool startBoss;
    public static BossAnimationManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        startBoss = false;
        bossAnimation.SetActive(true);
        boss.SetActive(false);
        StartCoroutine(BossAnimating());
    }

    IEnumerator BossAnimating()
    {
        yield return new WaitForSeconds(animationTime);
        bossAnimation.SetActive(false);
        boss.SetActive(true);
        startBoss = true;
    }
}
