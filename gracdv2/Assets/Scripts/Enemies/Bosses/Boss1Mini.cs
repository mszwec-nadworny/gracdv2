using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1Mini : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform[] bulletSpawns;
    public GameObject bulletPrefab;
    private bool canUShoot;

    [SerializeField] private ParticleSystem ps;
    private EnemyHpManager eHpM;

    public GameObject hpUI;
    public Image hpBar;
    public Boss1Manager bm;
    public int bossIndex;
    public GameObject shield;
    public float shieldTime;
    public float shieldCooldown;
    public GameObject boss;
    private void Start()
    {
        canUShoot = true;
        eHpM = GetComponent<EnemyHpManager>();
        UIManager.Instance.bossHP = eHpM.currentHP;
        UIManager.Instance.isBossHere = true;
        hpUI.SetActive(true);
        GetCurrentFill();
        StopAllCoroutines();
        if(bossIndex == 1)
        {
            StartCoroutine(ShieldOn());
        }else if(bossIndex == 2)
        {
            shield.SetActive(false);
            StartCoroutine(ShieldCooldown());
        }else { Debug.Log("Wrong boss index"); }
    }

    // movement
    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            Shoot();
        }

        GetCurrentFill();

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("boss1");
            SicknessManager.Instance.killedEnemies++;
            if(bossIndex == 1)
            {
                bm.isMiniBoss1Dead = true;
            }else if(bossIndex == 2)
            {
                bm.isMiniBoss2Dead = true;
            }else { Debug.Log("Wrong boss index"); }
            boss.SetActive(false);
        }
    }

    private void Shoot()
    {
        canUShoot = false;
        foreach (Transform bulletSpawn in bulletSpawns)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        }

        StartCoroutine(CantShoot());
    }
    IEnumerator CantShoot()
    {
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }

    private void GetCurrentFill()
    {
        float fill = (float)eHpM.currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }

    IEnumerator ShieldOn()
    {
        shield.SetActive(true);
        hpUI.SetActive(false);
        yield return new WaitForSeconds(shieldTime);
        shield.SetActive(false);
        hpUI.SetActive(true);
        StartCoroutine(ShieldCooldown());
    }
    IEnumerator ShieldCooldown()
    {
        yield return new WaitForSeconds(shieldCooldown);
        StartCoroutine(ShieldOn());
    }
}
