using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2Minion : MonoBehaviour
{
    public EnemiesShooting esConfig;
    private int currentHP;
    public Transform bulletSpawn;
    public GameObject bulletPrefab;
    [HideInInspector] public bool canUShoot;

    [SerializeField] private ParticleSystem ps;

    public float shootSpeedSmall = 0.2f;
    private void Start()
    {
        canUShoot = false;
        currentHP = esConfig.hpMax;
        StopAllCoroutines();
        StartCoroutine(CantShoot());
    }

    // movement
    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            StartCoroutine(Shoot());
        }

        // killed and raise sickness
        if (currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Enemy"))
        {
            currentHP -= 1;
            Destroy(other.gameObject);
        }
    }
    IEnumerator Shoot()
    {
        canUShoot = false;
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        yield return new WaitForSeconds(shootSpeedSmall);
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        yield return new WaitForSeconds(shootSpeedSmall);
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        StartCoroutine(CantShoot());
        //Rigidbody rig = cb.GetComponent<Rigidbody>();

        //rig.AddForce(new Vector3(-1,0,0)*esConfig.bulletSpeed, ForceMode.Impulse);
    }
    IEnumerator CantShoot()
    {
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
}
