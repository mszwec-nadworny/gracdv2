using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.UI;

public class Boss3WeakPoint : MonoBehaviour
{
    public Boss3 boss3;
    [SerializeField] private Image hpBar;
    private int currentHP;
    [SerializeField] private ParticleSystem psDestroy;
    void Start()
    {
        currentHP = boss3.weakPointHP;
        GetCurrentFill();
    }

    void Update()
    {
        GetCurrentFill();
        if (currentHP <= 0)
        {
            boss3.destroyedWeakPoints++;
            psDestroy.Play();
            this.gameObject.SetActive(false);
        }
    }

    [System.Obsolete]
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            currentHP -= PlayerManager.Instance.playerCurrentDmg;
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Explode"))
        {
            other.GetComponentInChildren<Bullet_Explode>().BulletExplode();
            currentHP -= PlayerManager.Instance.playerCurrentDmg;
        }
    }
    public void ResetWeakPoint()
    {
        currentHP = boss3.weakPointHP;
        GetCurrentFill();
    }
    private void GetCurrentFill()
    {
        float fill = (float)currentHP / (float)boss3.weakPointHP;
        hpBar.fillAmount = fill;
    }
}
