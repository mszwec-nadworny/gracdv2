using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.UI;

public class Boss2Behaviour : MonoBehaviour
{
    [SerializeField] private EnemiesShooting esConfig;
    private int currentHP;
    [SerializeField] private ParticleSystem psDeath;
    [SerializeField] private GameObject ripple1;
    [SerializeField] private GameObject ripple2;
    [SerializeField] private Image hpBar;

    [SerializeField] private List<GameObject> minionSpawns;
    [SerializeField] private GameObject minion1;
    [SerializeField] private List<Transform> bulletSpawns;
    [SerializeField] private GameObject bullet;
    [SerializeField] private float vulnerableTime;
    [SerializeField] private int explosionCounter = 3;
    private bool explosion1, explosion2;
    [SerializeField] private float explosionTresshold1, explosionTresshold2;
    private int vulnerability; // 1 - normal, 2 - immune, 3 - vulnerable
    private void Start()
    {
        currentHP = esConfig.hpMax;
        UIManager.Instance.bossHP = currentHP;
        UIManager.Instance.isBossHere = true;
        GetCurrentFill();
        foreach(GameObject minion in minionSpawns)
        {
            //Instantiate(minion1, minion.position, Quaternion.identity);
            minion.SetActive(true);
        }
        explosion1 = false; 
        explosion2 = false;
        ripple1.SetActive(true);
        ripple2.SetActive(false);
        vulnerability = 1;
        StopAllCoroutines();
    }
    void Update()
    {
        GetCurrentFill();
        if((currentHP < (esConfig.hpMax * explosionTresshold1)) && !explosion1)
        {
            explosion1 = true;
            // send waves
            StartCoroutine(ExplosionWave());
        }
        if((currentHP < (esConfig.hpMax * explosionTresshold2)) && !explosion2)
        {
            explosion2 = true;
            StartCoroutine(ExplosionWave());
        }
        // killed and raise sickness
        if (currentHP <= 0)
        {
            psDeath.Play();
            AudioManager.Instance.PlaySound("boss2");
            SicknessManager.Instance.killedEnemies++;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // lose hp when shot
        if (other.CompareTag("Bullet_Player"))
        {
            if(vulnerability == 1)
            {
                currentHP -= PlayerManager.Instance.playerCurrentDmg;
                UIManager.Instance.bossHP = currentHP;
            }else if(vulnerability == 3)
            {
                currentHP -= PlayerManager.Instance.playerCurrentDmg * 2;
                UIManager.Instance.bossHP = currentHP;
            }
            Destroy(other.gameObject);
        }
    }
    private void GetCurrentFill()
    {
        float fill = (float)currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }
    private void Shoot()
    {
        foreach (Transform bulletSpawn in bulletSpawns)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
        }
    }
    IEnumerator ExplosionWave()
    {
        ripple1.SetActive(false);
        ripple2.SetActive(true);
        vulnerability = 2;
        yield return new WaitForSeconds(vulnerableTime/2);
        ripple1.SetActive(true);
        ripple2.SetActive(false);
        foreach(GameObject minion in minionSpawns)
        {
            minion.SetActive(false);
        }
        for(int i=0; i<explosionCounter; i++)
        {
            Shoot();
            yield return new WaitForSeconds(esConfig.shootSpeed);
        }
        vulnerability = 3;
        yield return new WaitForSeconds(vulnerableTime);
        vulnerability = 1;
        foreach (GameObject minion in minionSpawns)
        {
            //Instantiate(minion1, minion.position, Quaternion.identity);
            minion.SetActive(true);
            minion.GetComponentInChildren<Boss2Minion>().canUShoot = true;
        }
    }
}
