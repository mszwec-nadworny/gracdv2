using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1 : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform[] bulletSpawns;
    public GameObject bulletPrefab;
    private bool canUShoot;

    [SerializeField] private ParticleSystem ps;
    private EnemyHpManager eHpM;

    public Image hpBar;

    public GameObject miniBoss1;
    public GameObject miniBoss2;

    public GameObject boss;
    public int shootCounter = 3;
    public float shootSmallDelay = .2f;
    private void Start()
    {
        miniBoss1.SetActive(false);
        miniBoss2.SetActive(false);
        canUShoot = true;
        eHpM = GetComponent<EnemyHpManager>();
        UIManager.Instance.bossHP = eHpM.currentHP;
        UIManager.Instance.isBossHere = true;
        GetCurrentFill();
        StopAllCoroutines();
    }

    // movement
    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            StartCoroutine(Shooting());
        }

        GetCurrentFill();

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("bossSplit1");
            SicknessManager.Instance.killedEnemies++;
            // spawn small bosses
            miniBoss1.SetActive(true);
            miniBoss2.SetActive(true);
            boss.SetActive(false);
        }
    }

    private void Shoot()
    {
        canUShoot = false;
        foreach (Transform bulletSpawn in bulletSpawns)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        }
    }
    IEnumerator CantShoot()
    {
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
    IEnumerator Shooting()
    {
        for (int i = 0; i < shootCounter; i++)
        {
            Shoot();
            yield return new WaitForSeconds(shootSmallDelay);
        }        
        StartCoroutine(CantShoot());
    }
    private void GetCurrentFill()
    {
        float fill = (float)eHpM.currentHP / (float)esConfig.hpMax;
        hpBar.fillAmount = fill;
    }
}
