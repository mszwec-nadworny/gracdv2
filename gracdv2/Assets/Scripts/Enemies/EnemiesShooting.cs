using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Shooting", menuName = "ScriptableObject/Enemy/Shooting")]
public class EnemiesShooting : ScriptableObject
{
    public string enemyName;
    public int hpMax;
    public float moveSpeed;
    public float sicknessRise;
    public float sicknessIfNotKilled;
    public int damageOnTouch;
    public float betweenDamageTime;

    public float shootSpeed;
    public float bulletSpeed;
    public float points;
}
