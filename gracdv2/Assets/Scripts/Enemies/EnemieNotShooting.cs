using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Enemy Not Shooting", menuName = "ScriptableObject/Enemy/NotShooting")]
public class EnemieNotShooting : ScriptableObject
{
    public string enemyName;
    public int hpMax;
    public float moveSpeed;
    public float sicknessRise;
    public float sicknessIfNotKilled;
    public int damageOnTouch;
    public float betweenDamageTime;
    public float speedDebuff;
    public float speedDebuffTime;
    public float points;
}
