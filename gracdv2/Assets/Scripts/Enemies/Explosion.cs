using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public EnemieNotShooting ensConfig;
    public float explosionRadius = 3.16f;
    private RaycastHit[] hits;
    private Vector3 startPosition;
    private void Start()
    {
        startPosition = this.transform.position;
        
        hits = Physics.SphereCastAll(startPosition, explosionRadius, transform.forward, 0);
        
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Player") && !PlayerManager.Instance.isImmune && !PlayerManager.Instance.takeNoDmg)
            {
                if (PlayerManager.Instance.isShieldON)
                {
                    PlayerManager.Instance.DestroyShield();
                    PlayerManager.Instance.playerCurrentHP -= ensConfig.damageOnTouch - 1;
                    SicknessManager.Instance.damageReceived += ensConfig.damageOnTouch - 1;
                }
                else
                {
                    PlayerManager.Instance.playerCurrentHP -= ensConfig.damageOnTouch;
                    SicknessManager.Instance.damageReceived += ensConfig.damageOnTouch;
                }
            }
            if (hit.collider.CompareTag("Enemy"))
            {
                if (hit.collider.GetComponent<EnemyHpManager>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManager>().currentHP -= ensConfig.damageOnTouch;
                }
                else if (hit.collider.GetComponent<EnemyHpManagerNotShooting>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManagerNotShooting>().currentHP -= ensConfig.damageOnTouch;
                }
                else if (hit.collider.GetComponent<ExplodingEnemy>() != null)
                {
                    hit.collider.GetComponent<ExplodingEnemy>().currentHP -= ensConfig.damageOnTouch;
                }
            }
        }
    }
}
