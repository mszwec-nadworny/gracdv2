using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlue : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform bulletSpawn;
    public GameObject bulletPrefab;
    private bool canUShoot;

    [SerializeField] private ParticleSystem ps;
    private EnemyHpManager eHpM;

    public float shootSpeedSmall = 0.2f;
    private void Start()
    {
        canUShoot = false;
        eHpM = GetComponent<EnemyHpManager>();
        StopAllCoroutines();
    }

    // movement
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * esConfig.moveSpeed, Space.World);
        ps.transform.Translate(Vector3.left * Time.deltaTime * esConfig.moveSpeed, Space.World);

        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            StartCoroutine(Shoot());
        }

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += esConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += esConfig.points;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // sickness if not killed

        if (other.CompareTag("LeftBorder") && esConfig.sicknessIfNotKilled != 0)
        {
            AudioManager.Instance.PlaySound("infectionLoss");
            SicknessManager.Instance.currentSicknessLvl -= esConfig.sicknessIfNotKilled;
            SicknessManager.Instance.CheckDifficulty();
        }
        if (other.CompareTag("LeftBorder"))
        {
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        // start shooting
        if (other.CompareTag("Right_Border"))
        {
            canUShoot = true;
        }
    }

    IEnumerator Shoot()
    {
        canUShoot = false;
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        yield return new WaitForSeconds(shootSpeedSmall);
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        yield return new WaitForSeconds(shootSpeedSmall);
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        StartCoroutine(CantShoot());
        //Rigidbody rig = cb.GetComponent<Rigidbody>();

        //rig.AddForce(new Vector3(-1,0,0)*esConfig.bulletSpeed, ForceMode.Impulse);
    }
    IEnumerator CantShoot()
    {
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
}
