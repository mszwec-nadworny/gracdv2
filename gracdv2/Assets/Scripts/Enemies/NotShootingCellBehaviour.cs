using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class NotShootingCellBehaviour : MonoBehaviour
{
    public EnemieNotShooting ensConfig;
    private bool waitForDamage;
    [SerializeField] private ParticleSystem ps;
    private EnemyHpManagerNotShooting eHpM;
    private void Start()
    {
        waitForDamage = false;
        eHpM = GetComponent<EnemyHpManagerNotShooting>();
    }

    // movement
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * ensConfig.moveSpeed, Space.World);

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += ensConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += ensConfig.points;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {    
        // damage on touch

        if (other.CompareTag("Player") && !PlayerManager.Instance.takeNoDmg && ensConfig.damageOnTouch  != 0 && !PlayerManager.Instance.isImmune)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= ensConfig.damageOnTouch;
                SicknessManager.Instance.damageReceived++;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }   
        
        // sickness if not killed

        if (other.CompareTag("LeftBorder") && ensConfig.sicknessIfNotKilled != 0)
        {
            AudioManager.Instance.PlaySound("infectionLoss");
            SicknessManager.Instance.currentSicknessLvl -= ensConfig.sicknessIfNotKilled;
            SicknessManager.Instance.CheckDifficulty();
        }
    }
    // damage if you stay near

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !waitForDamage && !PlayerManager.Instance.takeNoDmg && ensConfig.damageOnTouch != 0 && !PlayerManager.Instance.isImmune )
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= ensConfig.damageOnTouch;
                SicknessManager.Instance.damageReceived++;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            waitForDamage = false;
            StopAllCoroutines();
        }
    }
    IEnumerator WaitForDamage()
    {
        yield return new WaitForSeconds(ensConfig.betweenDamageTime);
        waitForDamage = false;
    }
}
