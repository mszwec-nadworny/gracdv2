using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class ShootingCellBehaviour : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform bulletSpawn;
    public GameObject bulletPrefab;
    private bool canUShoot;

    private bool waitForDamage;
    [SerializeField] private ParticleSystem ps;
    private EnemyHpManager eHpM;
    private void Start()
    {
        canUShoot = false;
        eHpM = GetComponent<EnemyHpManager>();
        StopAllCoroutines();
    }

    // movement
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * esConfig.moveSpeed, Space.World);

        if(canUShoot && !PlayerManager.Instance.maskOn)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            Shoot();
            StartCoroutine(CantShoot());
        }

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += esConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += esConfig.points;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // damage on touch

        if (other.CompareTag("Player") && !PlayerManager.Instance.takeNoDmg && esConfig.damageOnTouch != 0)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }

        // sickness if not killed

        if (other.CompareTag("LeftBorder") && esConfig.sicknessIfNotKilled != 0)
        {
            AudioManager.Instance.PlaySound("infectionLoss");
            SicknessManager.Instance.currentSicknessLvl -= esConfig.sicknessIfNotKilled;
            SicknessManager.Instance.CheckDifficulty();
        }
        if (other.CompareTag("LeftBorder"))
        {
            this.gameObject.SetActive(false);
        }       
    }
    private void OnTriggerExit(Collider other)
    {
        // start shooting
        if (other.CompareTag("Right_Border"))
        {
            canUShoot= true;
        }
    }

    // damage if you stay near

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !waitForDamage && !PlayerManager.Instance.takeNoDmg && esConfig.damageOnTouch != 0)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= esConfig.damageOnTouch;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            waitForDamage = true;
            StartCoroutine(WaitForDamage());
        }
    }

    IEnumerator WaitForDamage()
    {
        yield return new WaitForSeconds(esConfig.betweenDamageTime);
        waitForDamage = false;
    }

    private void Shoot()
    {
        GameObject cb = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity) as GameObject;
        //Rigidbody rig = cb.GetComponent<Rigidbody>();

        //rig.AddForce(new Vector3(-1,0,0)*esConfig.bulletSpeed, ForceMode.Impulse);
    }
    IEnumerator CantShoot()
    {
        canUShoot = false;
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
}
