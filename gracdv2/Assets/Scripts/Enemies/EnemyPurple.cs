using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class EnemyPurple : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform bulletSpawn;
    public GameObject bulletPrefab;
    private bool canUShoot;

    [SerializeField] private ParticleSystem ps;

    public GameObject shield;
    public float shootingSpeedDebuff;
    public float shootingSpeedDebuffTime;

    private EnemyHpManager eHpM;
    private void Start()
    {
        canUShoot = false;
        StopAllCoroutines();
        eHpM = GetComponent<EnemyHpManager>();
        shield.SetActive(true);
        eHpM.currentHP += 2;
    }

    // movement
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * esConfig.moveSpeed, Space.World);

        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            AudioManager.Instance.PlaySound("bulletEnemy");
            StartCoroutine(CantShoot());
            Shoot();
        }

        // shield of
        if(eHpM.currentHP == esConfig.hpMax)
        {
            shield.SetActive(false);
        }

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += esConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += esConfig.points;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // sickness if not killed

        if (other.CompareTag("LeftBorder") && esConfig.sicknessIfNotKilled != 0)
        {
            AudioManager.Instance.PlaySound("infectionLoss");
            SicknessManager.Instance.currentSicknessLvl -= esConfig.sicknessIfNotKilled;
            SicknessManager.Instance.CheckDifficulty();
        }
        if (other.CompareTag("LeftBorder"))
        {
            this.gameObject.SetActive(false);
        }

        // debuff for player shooting speed
        if (other.CompareTag("Player"))
        {
            if (!Player_Shooter.Instance.isDebuffed)
            {
                Player_Shooter.Instance.ShootingSpeedDebuff(shootingSpeedDebuff, shootingSpeedDebuffTime);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        // start shooting
        if (other.CompareTag("Right_Border"))
        {
            canUShoot = true;
        }
    }

    private void Shoot()
    {
        GameObject cb = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity) as GameObject;
        //Rigidbody rig = cb.GetComponent<Rigidbody>();

        //rig.AddForce(new Vector3(-1,0,0)*esConfig.bulletSpeed, ForceMode.Impulse);
    }
    IEnumerator CantShoot()
    {
        canUShoot = false;
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
}
