using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySplitBig : MonoBehaviour
{
    public EnemiesShooting esConfig;

    public Transform bulletSpawn;
    public GameObject bulletPrefab;
    private bool canUShoot;

    [SerializeField] private ParticleSystem ps;
    private EnemyHpManager eHpM;

    public GameObject split1;
    public GameObject split2;
    private void Awake()
    {
        split1.SetActive(false);
        split2.SetActive(false);
    }

    private void Start()
    {
        canUShoot = false;
        eHpM = GetComponent<EnemyHpManager>();
        StopAllCoroutines();
    }

    // movement
    void Update()
    {
        if (canUShoot && !PlayerManager.Instance.maskOn)
        {
            Shoot();
        }

        // killed and raise sickness
        if (eHpM.currentHP <= 0)
        {
            ps.Play();
            AudioManager.Instance.PlaySound("kill");
            SicknessManager.Instance.currentSicknessLvl += esConfig.sicknessRise;
            SicknessManager.Instance.CheckDifficulty();
            SicknessManager.Instance.killedEnemies++;
            PlayerManager.Instance.startingPoints += esConfig.points;
            split1.SetActive(true);
            split2.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // sickness if not killed

        if (other.CompareTag("LeftBorder") && esConfig.sicknessIfNotKilled != 0)
        {
            AudioManager.Instance.PlaySound("infectionLoss");
            SicknessManager.Instance.currentSicknessLvl -= esConfig.sicknessIfNotKilled;
            SicknessManager.Instance.CheckDifficulty();
        }
        if (other.CompareTag("LeftBorder"))
        {
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        // start shooting
        if (other.CompareTag("Right_Border"))
        {
            canUShoot = true;
        }
    }

    private void Shoot()
    {
        canUShoot = false;
        AudioManager.Instance.PlaySound("bulletEnemy");
        Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);

        StartCoroutine(CantShoot());
    }
    IEnumerator CantShoot()
    {
        yield return new WaitForSeconds(esConfig.shootSpeed);
        canUShoot = true;
    }
}
