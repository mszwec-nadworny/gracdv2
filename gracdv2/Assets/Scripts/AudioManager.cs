using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioClip boostPick1, bossSplit1, boss1, boss2, boss3, boss4, bulletEnemy, bulletPlayer, 
        buttonBestiaryOpen, buttonBestiaryClose, buttonBestiaryHover, buttonPage, buttonInstructionHover, buttonInstruction,
        buttonClick, buttonHover, buttonUpgrades, damageEnemy, damagePlayer;
    public AudioClip defeat, difficultyChange, explosion, healing, infectionLoss, shield, victory;
    public List<AudioClip> killSounds = new List<AudioClip>();
    [HideInInspector] public AudioSource audioSrc;
    public AudioSource musicTheme;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        //audioSrc.Play();
    }
    public void PlaySound(string clip)
    {
        switch (clip)
        {
            case "boostPick1":
                audioSrc.PlayOneShot(boostPick1);
                break;
            case "bossSplit1":
                audioSrc.PlayOneShot(bossSplit1);
                break;
            case "boss1":
                audioSrc.PlayOneShot(boss1);
                break;
            case "boss2":
                audioSrc.PlayOneShot(boss2);
                break;
            case "boss3":
                audioSrc.PlayOneShot(boss3);
                break;
            case "boss4":
                audioSrc.PlayOneShot(boss4);
                break;
            case "bulletEnemy":
                audioSrc.PlayOneShot(bulletEnemy);
                break;
            case "bulletPlayer":
                audioSrc.PlayOneShot(bulletPlayer);
                break;
            case "buttonBestiaryOpen":
                audioSrc.PlayOneShot(buttonBestiaryOpen);
                break;
            case "buttonBestiaryClose":
                audioSrc.PlayOneShot(buttonBestiaryClose);
                break;
            case "buttonBestiaryHover":
                audioSrc.PlayOneShot(buttonBestiaryHover);
                break;
            case "buttonPage":
                audioSrc.PlayOneShot(buttonPage);
                break;
            case "buttonInstructionHover":
                audioSrc.PlayOneShot(buttonInstructionHover);
                break;
            case "buttonInstruction":
                audioSrc.PlayOneShot(buttonInstruction);
                break;
            case "button":
                audioSrc.PlayOneShot(buttonClick);
                break;
            case "buttonHover":
                audioSrc.PlayOneShot(buttonHover);
                break;
            case "buttonUpgrades":
                audioSrc.PlayOneShot(buttonUpgrades);
                break;
            case "damageEnemy":
                audioSrc.PlayOneShot(damageEnemy);
                break;
            case "damagePlayer":
                audioSrc.PlayOneShot(damagePlayer);
                break;
            case "defeat":
                audioSrc.PlayOneShot(defeat);
                break;
            case "difficultyChange":
                audioSrc.PlayOneShot(difficultyChange); 
                break;
            case "explosion":
                audioSrc.PlayOneShot(explosion);
                break;
            case "healing":
                audioSrc.PlayOneShot(healing);
                break;
            case "infectionLoss":
                audioSrc.PlayOneShot(infectionLoss);
                break;
            case "shield":
                audioSrc.PlayOneShot(shield);
                break;
            case "victory":
                audioSrc.PlayOneShot(victory); 
                break;
            case "kill":
                audioSrc.PlayOneShot(killSounds[Random.Range(0, killSounds.Count)]);
                break;

            default: break;
        }
    }
}
