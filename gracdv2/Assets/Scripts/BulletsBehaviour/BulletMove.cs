using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public EnemiesShooting esConfig;

    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * esConfig.bulletSpeed, Space.World);
    }
}
