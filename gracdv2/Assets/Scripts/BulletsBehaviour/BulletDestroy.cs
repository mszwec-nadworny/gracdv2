using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet_Player") || other.CompareTag("Bullet_Enemy") || other.CompareTag("Bullet_Explode"))
        {
            Destroy(other.gameObject);
        }
    }
}
