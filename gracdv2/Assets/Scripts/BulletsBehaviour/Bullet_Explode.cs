using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Explode : MonoBehaviour
{
    public float explosionRadius = 2f;
    public ParticleSystem explodePS;
    public GameObject parent;
    public GameObject child;
    private RaycastHit[] hits;
    private Vector3 startPosition;

    [System.Obsolete]
    public void BulletExplode()
    {        
        StartCoroutine(ExplodeParticle());
        startPosition = Vector3.zero;
        
        hits = Physics.SphereCastAll(startPosition, explosionRadius, transform.forward, 0);

        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                if (hit.collider.GetComponent<EnemyHpManager>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManager>().currentHP -= PlayerManager.Instance.playerCurrentDmg - 1;
                }
                else if (hit.collider.GetComponent<EnemyHpManagerNotShooting>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManagerNotShooting>().currentHP -= PlayerManager.Instance.playerCurrentDmg - 1;
                }
                else if (hit.collider.GetComponent<ExplodingEnemy>() != null)
                {
                    hit.collider.GetComponent<ExplodingEnemy>().currentHP -= PlayerManager.Instance.playerCurrentDmg - 1;
                }
            }
        }
        child.SetActive(false);
    }

    [System.Obsolete]
    IEnumerator ExplodeParticle()
    {
        explodePS.Play();
        yield return new WaitForSeconds(explodePS.duration);
        Destroy(parent);
    }
}