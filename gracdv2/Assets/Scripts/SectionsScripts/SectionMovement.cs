using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SectionMovement : MonoBehaviour
{
    [SerializeField] private SectionSO sso;
    private float sectionSpeed;

    private void Awake()
    {
        sectionSpeed = sso.sectionSpeed;
    }

    void Update()
    {
        transform.position -= new Vector3(sectionSpeed, 0, 0) * Time.deltaTime;
    }
}
