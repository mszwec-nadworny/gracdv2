using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionDestroy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("NewSection"))
        {
            Destroy(other.transform.parent.gameObject);
        }
    }
}
