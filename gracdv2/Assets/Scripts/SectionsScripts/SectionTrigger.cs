using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionTrigger : MonoBehaviour
{
    public static SectionTrigger Instance;
    //public GameObject newSection; // make it a list, a few lists
    [SerializeField] private List<GameObject> sectionsStart;
    [SerializeField] private List<GameObject> sectionsEasy;
    [SerializeField] private List<GameObject> sectionsMedium;
    [SerializeField] private List<GameObject> sectionsHard;
    [SerializeField] private List<GameObject> sectionsBoss;

    public float sicknessThresholdMedium;
    public float sicknessThresholdHard;
    public float sicknessThresholdBoss;

    private float newSectionSpawnX = 69f;
    private float newSectionSpawnY = -4.5f;

    [HideInInspector] public int lastSectionSpawnIndex;
    private int newSectionSpawnIndex;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        Instantiate(sectionsStart[Random.Range(0, sectionsStart.Count)], new Vector3(0, newSectionSpawnY, 0), Quaternion.identity);
        lastSectionSpawnIndex = -1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("NewSection"))
        {
            // Easy if current sickness level is less than Medium
            if (SicknessManager.Instance.currentDifficulty == Difficulty.Easy)
            {
                newSectionSpawnIndex = Random.Range(0, sectionsEasy.Count);
                if(newSectionSpawnIndex == lastSectionSpawnIndex)
                {
                    if(newSectionSpawnIndex != 0) newSectionSpawnIndex--;
                    else newSectionSpawnIndex = sectionsEasy.Count - 1;
                }
                lastSectionSpawnIndex = newSectionSpawnIndex;
                Instantiate(sectionsEasy[newSectionSpawnIndex], new Vector3(newSectionSpawnX, newSectionSpawnY, 0), Quaternion.identity);
            }
            // Medium if current sickness level is between Medium & Hard
            if (SicknessManager.Instance.currentDifficulty == Difficulty.Medium)
            {
                newSectionSpawnIndex = Random.Range(0, sectionsMedium.Count);
                if (newSectionSpawnIndex == lastSectionSpawnIndex)
                {
                    if (newSectionSpawnIndex != 0) newSectionSpawnIndex--;
                    else newSectionSpawnIndex = sectionsMedium.Count - 1;
                }
                lastSectionSpawnIndex = newSectionSpawnIndex;
                Instantiate(sectionsMedium[newSectionSpawnIndex], new Vector3(newSectionSpawnX, newSectionSpawnY, 0),Quaternion.identity);
            }
            // Hard if current sickness level is more than Hard
            if (SicknessManager.Instance.currentDifficulty == Difficulty.Hard)
            {
                newSectionSpawnIndex = Random.Range(0, sectionsHard.Count);
                if (newSectionSpawnIndex == lastSectionSpawnIndex)
                {
                    if (newSectionSpawnIndex != 0) newSectionSpawnIndex--;
                    else newSectionSpawnIndex = sectionsHard.Count - 1;
                }
                lastSectionSpawnIndex = newSectionSpawnIndex;
                Instantiate(sectionsHard[newSectionSpawnIndex], new Vector3(newSectionSpawnX, newSectionSpawnY, 0), Quaternion.identity);
            }
            // Boss if current sickness level is Max
            if (SicknessManager.Instance.currentDifficulty == Difficulty.Boss)
            {
                Instantiate(sectionsBoss[Random.Range(0, sectionsBoss.Count)], new Vector3(newSectionSpawnX, newSectionSpawnY, 0), Quaternion.identity);
            }
        }
    }
}
