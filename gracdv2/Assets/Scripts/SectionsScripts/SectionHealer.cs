using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SectionHealer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !UIManager.Instance.isBossHere)
        {
            HealerManager.Instance.SpawnHealer();
            this.gameObject.SetActive(false);
        }
    }
}
