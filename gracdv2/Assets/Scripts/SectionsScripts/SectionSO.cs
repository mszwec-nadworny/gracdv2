using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Section Stats", menuName = "ScriptableObject/SectionStats")]
public class SectionSO : ScriptableObject
{
    public float sectionSpeed;
}
