using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionBossTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !UIManager.Instance.isBossHere)
        {
            SicknessManager.Instance.spawnBoss = true;
            SicknessManager.Instance.restartPossible = true;
            TimeManager.Instance.preBossTime = TimeManager.Instance.currentTime;
        }
    }
}
