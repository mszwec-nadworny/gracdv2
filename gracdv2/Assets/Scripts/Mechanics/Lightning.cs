using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    [SerializeField] private float timeBetweenHits;
    [SerializeField] private float lightningTime;
    [SerializeField] private int damage;
    [SerializeField] private ParticleSystem psLightning;
    private bool dealDamage;

    private void Awake()
    {
        StartCoroutine(WaitForLightning());
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && dealDamage && !PlayerManager.Instance.takeNoDmg && !PlayerManager.Instance.isImmune)
        {
            if (PlayerManager.Instance.isShieldON)
            {
                PlayerShield.Instance.StartDestroyShield();
            }
            else
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                PlayerManager.Instance.playerCurrentHP -= damage;
                SicknessManager.Instance.damageReceived++;
                PlayerManager.Instance.TakeNoDmgFunction();
            }
            dealDamage = false;
        }
    }

    IEnumerator WaitForLightning()
    {
        dealDamage = false;
        yield return new WaitForSeconds(timeBetweenHits);
        dealDamage = true;
        psLightning.Play();
        yield return new WaitForSeconds(lightningTime);
        StartCoroutine(WaitForLightning());
    }
}
