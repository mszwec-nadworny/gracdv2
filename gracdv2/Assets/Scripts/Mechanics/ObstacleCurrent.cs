using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ObstacleCurrent : MonoBehaviour
{
    public float speedDecrease;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(speedDecrease > 0)
            {
                if(!PlayerManager.Instance.isDebuffed && !PlayerManager.Instance.isInWind)
                {
                    PlayerManager.Instance.playerCurrentSpeed -= speedDecrease;
                }
                InGameUI.Instance.debuffIcon.gameObject.SetActive(true);
                PlayerManager.Instance.isInWind = true;
            }
            else
            {
                PlayerManager.Instance.playerCurrentSpeed -= speedDecrease;
                InGameUI.Instance.speedBoostIcon.gameObject.SetActive(true);
                PlayerManager.Instance.speedBuffCounter++;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(speedDecrease > 0)
            {
                if(!PlayerManager.Instance.isDebuffed)
                {
                    InGameUI.Instance.debuffIcon.gameObject.SetActive(false);
                    PlayerManager.Instance.playerCurrentSpeed += speedDecrease;
                }
                PlayerManager.Instance.isInWind = false;
            }
            else if(speedDecrease < 0)
            {
                PlayerManager.Instance.playerCurrentSpeed += speedDecrease;
                if (PlayerManager.Instance.speedBuffCounter <= 1)
                {
                    InGameUI.Instance.speedBoostIcon.gameObject.SetActive(false);
                }
                PlayerManager.Instance.speedBuffCounter--;
            }
        }
    }
}
