using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingSpeedBoost : MonoBehaviour
{
    public float shootingSpeedBuffed;
    public float boostTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound("boostPick1");
            PlayerManager.Instance.shootingSpeedBoostCounter++;
            Player_Shooter.Instance.ShootingSpeedBuff(shootingSpeedBuffed, boostTime);
            this.gameObject.SetActive(false);
        }
    }
}
