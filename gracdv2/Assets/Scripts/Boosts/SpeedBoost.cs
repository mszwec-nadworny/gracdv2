using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour
{
    public float addedSpeed;
    public float boostTime;
    [HideInInspector] public bool isActive;
    public GameObject icon;

    private void Start()
    {
        isActive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!isActive && other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound("boostPick1");
            PlayerManager.Instance.SpeedBuff(addedSpeed, boostTime);
            isActive = true;
            icon.SetActive(false);
        }
    }
}
