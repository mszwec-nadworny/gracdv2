using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBoost : MonoBehaviour
{
    public List<GameObject> boostsList;
    void Start()
    {
        for(int i=0; i<boostsList.Count; i++)
        {
            boostsList[i].SetActive(false);
        }
        boostsList[Random.Range(0,boostsList.Count)].SetActive(true);
    }
}
