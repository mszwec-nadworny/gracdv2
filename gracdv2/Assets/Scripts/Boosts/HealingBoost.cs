using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingBoost : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(PlayerManager.Instance.playerCurrentHP < PlayerManager.Instance.playerStats.playerMaxHP)
            {
                AudioManager.Instance.PlaySound("healing");
                PlayerManager.Instance.playerCurrentHP++;
                InGameUI.Instance.HPDisplay();
            }
            this.gameObject.SetActive(false);
        }
    }
}
