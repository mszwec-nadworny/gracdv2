using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBoost : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound("shield");
            PlayerManager.Instance.ShieldON();
            this.gameObject.SetActive(false);
        }
    }
}
