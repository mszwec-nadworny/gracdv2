using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleShotBoost : MonoBehaviour
{
    public float boostTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound("boostPick1");
            PlayerManager.Instance.tripleShotBoostCounter++;
            Player_Shooter.Instance.TripleShotActivate(boostTime);
            this.gameObject.SetActive(false);
        }
    }
}
