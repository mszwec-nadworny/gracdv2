using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBoost : MonoBehaviour
{
    public int damageBoost;
    public float boostTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound("boostPick1");
            PlayerManager.Instance.damageBoostCounter++;
            PlayerManager.Instance.DmgBuff(damageBoost, boostTime);
            this.gameObject.SetActive(false);
        }
    }
}
