using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    /*public string PlayTheGame = "SampleScene";
    public string Credits = "Credits";
    public string Options = "Options";
    public string Bestiary = "Bestiary";
    public string Instruction = "Instruction";
    public string MainMenu = "MainMenu";
    */
    [SerializeField] private GameObject mainMenuButtons;
    [SerializeField] private GameObject credits;
    [SerializeField] private GameObject options;
    [SerializeField] private GameObject bestiary;
    [SerializeField] private GameObject instruction;
    [SerializeField] private OptionsUI optionsUI;
    private void Awake()
    {
        Time.timeScale = 1f;
        //OptionsUI.Instance.LoadVolume();
        optionsUI.LoadVolume();
        optionsUI.LoadMusic();
        credits.SetActive(false);
        options.SetActive(false);
        bestiary.SetActive(false);
        instruction.SetActive(false);
        mainMenuButtons.SetActive(true);
    }

    public void PlayGame()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(1);
    }
    public void PlayAction()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(2);
    }
    public void OpenCredits()
    {
        AudioManager.Instance.PlaySound("button");
        credits.SetActive(true);
        options.SetActive(false);
        bestiary.SetActive(false);
        instruction.SetActive(false);
        mainMenuButtons.SetActive(true);
    }

    public void OpenOptions()
    {
        AudioManager.Instance.PlaySound("button");
        options.SetActive(true);
        credits.SetActive(false);
        bestiary.SetActive(false);
        instruction.SetActive(false);
        mainMenuButtons.SetActive(false);
    }

    public void OpenBestiary()
    {
        AudioManager.Instance.PlaySound("buttonBestiaryOpen");
        bestiary.SetActive(true);
        credits.SetActive(false);
        options.SetActive(false);
        instruction.SetActive(false);
        mainMenuButtons.SetActive(true);
    }

    public void OpenInstructions()
    {
        AudioManager.Instance.PlaySound ("buttonInstruction");
        instruction.SetActive(true);
        credits.SetActive(false);
        options.SetActive(false);
        bestiary.SetActive(false);
        mainMenuButtons.SetActive(true);
    }

    public void MainMenuLoader()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        AudioManager.Instance.PlaySound("button");
        Application.Quit();
    }
}
