using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverUI : MonoBehaviour, IPointerEnterHandler
{
    public string sound;
    public void OnPointerEnter(PointerEventData eventData)
    {
        AudioManager.Instance.PlaySound(sound);
    }
}
