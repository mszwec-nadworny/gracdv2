using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [SerializeField] private GameObject pause;
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject upgradesScreen;
    [SerializeField] private GameObject endGameScreen;
    [SerializeField] private OptionsUI optionsUI;
    [SerializeField] private DeathZone dz;
    [HideInInspector] public bool isBossHere;
    [HideInInspector] public int bossHP = 10;
    public string hpLost;
    public string timeOut;
    public string infectionTooLow;
    public string deatchZone;
    [HideInInspector] public bool isOver;
    [HideInInspector] public bool isPaused;
    [HideInInspector] public bool upgradesON;
    [HideInInspector] public bool endGameON;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        Time.timeScale = 1f;
        //OptionsUI.Instance.LoadVolume();
        optionsUI.LoadVolume();
        optionsUI.LoadMusic();
        GameOverUI.Instance.gameObject.SetActive(false);
        pause.SetActive(false);
        winScreen.SetActive(false);
        upgradesScreen.SetActive(false);
        endGameScreen.SetActive(false);
        Cursor.visible= false;
        isBossHere = false;
        isOver = false;
        isPaused = false;
        upgradesON = false;
        endGameON = false;
    }

    private void Update()
    {
        // game over
        if(PlayerManager.Instance.playerCurrentHP <= 0)
        {
            if (!isOver)
            {            
                AudioManager.Instance.musicTheme.Stop();
                Time.timeScale = 0f;
                GameOverUI.Instance.gameObject.SetActive(true);
                GameOverUI.Instance.BossRestartVisible();
                Cursor.visible= true;
                AudioManager.Instance.PlaySound("defeat");
                isOver = true;            
                GameOverUI.Instance.deatchTypeTXT.text = hpLost;
            }
        }
        /*if (TimeManager.Instance.currentTime >= TimeManager.Instance.timeMax)
        {
            AudioManager.Instance.musicTheme.Stop();
            Time.timeScale = 0f;
            GameOverUI.Instance.gameObject.SetActive(true);
            Cursor.visible = true;
            if (!isOver)
            {
                AudioManager.Instance.PlaySound("defeat");
                isOver = true;
            }
            GameOverUI.Instance.deatchTypeTXT.text = timeOut;
        }*/
        if (SicknessManager.Instance.currentSicknessLvl <= SicknessManager.Instance.minSicknessLvl)
        {
            if (!isOver)
            {            
                AudioManager.Instance.musicTheme.Stop();
                Time.timeScale = 0f;
                GameOverUI.Instance.gameObject.SetActive(true);
                GameOverUI.Instance.BossRestartVisible();
                Cursor.visible = true;
                AudioManager.Instance.PlaySound("defeat");
                isOver = true;            
                GameOverUI.Instance.deatchTypeTXT.text = infectionTooLow;
            }
        }
        if (dz.isDead)
        {
            if (!isOver)
            {            
                AudioManager.Instance.musicTheme.Stop();
                Time.timeScale = 0f;
                GameOverUI.Instance.gameObject.SetActive(true);
                GameOverUI.Instance.BossRestartVisible();
                Cursor.visible = true;
                AudioManager.Instance.PlaySound("defeat");
                isOver = true;            
                GameOverUI.Instance.deatchTypeTXT.text = deatchZone;
            }
        }
        // win screen
        if (isBossHere && bossHP <= 0 && !upgradesON && !endGameON)
        {                
            if (!isOver)
            {
                AudioManager.Instance.musicTheme.Stop();
                winScreen.SetActive(true);
                winScreen.GetComponent<WinUI>().DisplayAll();
                Time.timeScale = 0f;
                Cursor.visible = true;
                AudioManager.Instance.PlaySound("victory");
                isOver = true;
            }
        }
        // upgrades screen
        if(upgradesON)
        {
            winScreen.SetActive(false);
            upgradesScreen.SetActive(true);
        }
        if (endGameON)
        {
            winScreen.SetActive(false);
            endGameScreen.SetActive(true);
            EndGameUI.Instance.DisplayAll();
        }
        // pause
        if(Input.GetKeyUp(KeyCode.Escape) && !isOver)
        {
            isPaused = true;
            Time.timeScale = 0f;
            pause.SetActive(true);
            Cursor.visible= true;
        }
    }
}
