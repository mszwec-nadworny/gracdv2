using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    public static OptionsUI Instance;

    [SerializeField] private Slider sfxSlider;
    [SerializeField] private AudioSource asSFX;
    private float sfxVolume;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private AudioSource asMusic;
    private float musicVolume;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("musicVolume"))
        {
            LoadMusic();
        }
        if (PlayerPrefs.HasKey("sfxVolume"))
        {
            LoadVolume();
        }
        SFXVolume();
        MusicVolume();
    }
    public void SFXVolume()
    {
        sfxVolume = sfxSlider.value;
        asSFX.volume = sfxVolume;
        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
    }
    public void MusicVolume()
    {
        musicVolume = musicSlider.value;
        asMusic.volume = musicVolume;
        PlayerPrefs.SetFloat("musicVolume", musicVolume);
    }
    public void LoadMusic()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        MusicVolume();
    }
    public void LoadVolume()
    {
        sfxSlider.value = PlayerPrefs.GetFloat("sfxVolume");
        SFXVolume();
    }
}
