using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bestiary : MonoBehaviour
{
    public GameObject page1;
    public GameObject page2;
    public GameObject page3;
    public GameObject page4;

    private void Start()
    {
        page1.SetActive(true);
        page2.SetActive(false);
        page3.SetActive(false);
        page4.SetActive(false);
    }
    public void Page1To2()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page1.SetActive(false);
        page2.SetActive(true);
    }
    public void Page2To1()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page1.SetActive(true);
        page2.SetActive(false);
    }
    public void Page2To3()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page2.SetActive(false);
        page3.SetActive(true);
    }
    public void Page3To2()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page2.SetActive(true);
        page3.SetActive(false);
    }
    public void Page3To4()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page3.SetActive(false);
        page4.SetActive(true);
    }
    public void Page4To3()
    {
        AudioManager.Instance.PlaySound("buttonPage");
        page3.SetActive(true);
        page4.SetActive(false);
    }
}
