using Mono.Cecil.Cil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseUI : MonoBehaviour
{
    public static PauseUI Instance;

    [SerializeField] private GameObject buttons;
    [SerializeField] private GameObject instruction;
    [SerializeField] private GameObject bestiary;
    [SerializeField] private GameObject options;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        instruction.SetActive(false);
        bestiary.SetActive(false);
        options.SetActive(false);
        buttons.SetActive(true);
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {   
            UIManager.Instance.isPaused = false;
            instruction.SetActive(false);
            bestiary.SetActive(false);
            options.SetActive(false);
            buttons.SetActive(true);
            Cursor.visible = false;               
            Time.timeScale = 1f;     
            this.gameObject.SetActive(false);
        }
    }
    public void ResumeGame()
    {
        AudioManager.Instance.PlaySound("button");
        Time.timeScale = 1f;        
        UIManager.Instance.isPaused = false;
        this.gameObject.SetActive(false);
        Cursor.visible= false;
    }
    public void Restart()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GoToMainMenu()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(0);
    }

    public void Bestiary()
    {
        AudioManager.Instance.PlaySound("buttonBestiaryOpen");
        instruction.SetActive(false);
        bestiary.SetActive(true);
        options.SetActive(false);
    }

    public void Instruction()
    {
        AudioManager.Instance.PlaySound("buttonInstruction");
        instruction.SetActive(true);
        bestiary.SetActive(false);
        options.SetActive(false);
    }

    public void Options()
    {
        AudioManager.Instance.PlaySound("button");
        instruction.SetActive(false);
        bestiary.SetActive(false);
        options.SetActive(true);
        buttons.SetActive(false);
    }
}
