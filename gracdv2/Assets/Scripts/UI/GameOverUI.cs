using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverUI : MonoBehaviour
{
    public static GameOverUI Instance;

    [SerializeField] private GameObject bossRestartButton;
    [SerializeField] private GameObject instruction;
    [SerializeField] private GameObject bestiary;
    public TextMeshProUGUI deatchTypeTXT;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        instruction.SetActive(false);
        bestiary.SetActive(false);
        bossRestartButton.SetActive(false);
    }
    public void BossRestartVisible()
    {
        if (SicknessManager.Instance.restartPossible || SicknessManager.Instance.restartStats.level1Points == -1)
        {
            bossRestartButton.SetActive(true);
        }
        else
        {
            bossRestartButton.SetActive(false);
        }
    }
    public void BossRestart()
    {
        if(TimeManager.Instance.preBossTime > 0)
        {
            SicknessManager.Instance.restartStats.time = TimeManager.Instance.preBossTime;
        }
        else
        {
            SicknessManager.Instance.restartStats.time = TimeManager.Instance.currentTime;
        }
        SicknessManager.Instance.restartStats.enemiesKilled = SicknessManager.Instance.killedEnemies;
        SicknessManager.Instance.restartStats.damageReceived = SicknessManager.Instance.damageReceived;
        SicknessManager.Instance.restartStats.stationsInfected = SicknessManager.Instance.infectedStations;
        SicknessManager.Instance.restartStats.level1Points = -1;
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Restart()
    {
        SicknessManager.Instance.restartStats.time = 0;
        SicknessManager.Instance.restartStats.enemiesKilled = 0;
        SicknessManager.Instance.restartStats.damageReceived = 0;
        SicknessManager.Instance.restartStats.stationsInfected = 0;
        SicknessManager.Instance.restartStats.level1Points = 0;
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GoToMainMenu()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(0);
    }

    public void Bestiary()
    {
        AudioManager.Instance.PlaySound("buttonBestiaryOpen");
        instruction.SetActive(false);
        bestiary.SetActive(true);
    }

    public void Instruction()
    {
        AudioManager.Instance.PlaySound("buttonInstruction");
        instruction.SetActive(true);
        bestiary.SetActive(false);
    }
}
