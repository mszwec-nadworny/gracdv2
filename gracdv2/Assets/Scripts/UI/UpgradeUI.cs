using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpgradeUI : MonoBehaviour
{
    public static UpgradeUI Instance;

    [SerializeField] private string nextLevelName;
    [SerializeField] private int upgradePoints;
    [SerializeField] private UpgradesSO upgradesList;
    [SerializeField] private PlayerSO playerCurrentStats;
    [SerializeField] private TextMeshProUGUI pointsCounter;

    [SerializeField] private GameObject shieldButton;
    [SerializeField] private List<GameObject> shieldList;
    [SerializeField] private GameObject savingShieldButton;
    [SerializeField] private List<GameObject> savingShieldList;
    [SerializeField] private GameObject extraToughButton;
    [SerializeField] private List<GameObject> extraToughList;

    [SerializeField] private GameObject fasterShootButton;
    [SerializeField] private List<GameObject> fasterShootList;
    [SerializeField] private GameObject powerUpButton;
    [SerializeField] private List<GameObject> powerUpList;
    [SerializeField] private GameObject bulletsButton;
    [SerializeField] private List<GameObject> bulletsList;

    [SerializeField] private GameObject boosterButton;
    [SerializeField] private List<GameObject> boosterList;
    [SerializeField] private GameObject maskingButton;
    [SerializeField] private List<GameObject> maskingList;
    [SerializeField] private GameObject bombButton;
    [SerializeField] private List<GameObject> bombList;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        /////////////////////////////////////
        if (upgradesList.shieldUpgrade) 
        { 
            shieldButton.SetActive(false);
            shieldList[0].SetActive(false);
            shieldList[1].SetActive(false);
            shieldList[2].SetActive(true);
            savingShieldButton.SetActive(true);
            savingShieldList[0].SetActive(false);
            savingShieldList[1].SetActive(true);
            savingShieldList[2].SetActive(false);
            extraToughButton.SetActive(false);
            extraToughList[0].SetActive(true);
            extraToughList[1].SetActive(false);
            extraToughList[2].SetActive(false);
        }
        else
        {
            shieldButton.SetActive(true);
            shieldList[0].SetActive(false);
            shieldList[1].SetActive(true);
            shieldList[2].SetActive(false);
            savingShieldButton.SetActive(false);
            savingShieldList[0].SetActive(true);
            savingShieldList[1].SetActive(false);
            savingShieldList[2].SetActive(false);
            extraToughButton.SetActive(false);
            extraToughList[0].SetActive(true);
            extraToughList[1].SetActive(false);
            extraToughList[2].SetActive(false);
        }
        if(upgradesList.savingShieldUpgrade)
        {
            savingShieldButton.SetActive(false);
            savingShieldList[0].SetActive(false);
            savingShieldList[1].SetActive(false);
            savingShieldList[2].SetActive(true);
            extraToughButton.SetActive(true);
            extraToughList[0].SetActive(false);
            extraToughList[1].SetActive(true);
            extraToughList[2].SetActive(false);
        }
        if (upgradesList.extraToughUpgrade)
        {
            extraToughButton.SetActive(false);
            extraToughList[0].SetActive(false);
            extraToughList[1].SetActive(false);
            extraToughList[2].SetActive(true);
        }
        /////////////////////////////////////
        if (upgradesList.fasterShootUpgrade)
        {
            fasterShootButton.SetActive(false);
            fasterShootList[0].SetActive(false);
            fasterShootList[1].SetActive(false);
            fasterShootList[2].SetActive(true);
            powerUpButton.SetActive(true);
            powerUpList[0].SetActive(false);
            powerUpList[1].SetActive(true);
            powerUpList[2].SetActive(false);
            bulletsButton.SetActive(false);
            bulletsList[0].SetActive(true);
            bulletsList[1].SetActive(false);
            bulletsList[2].SetActive(false);
        }
        else
        {
            fasterShootButton.SetActive(true);
            fasterShootList[0].SetActive(false);
            fasterShootList[1].SetActive(true);
            fasterShootList[2].SetActive(false);
            powerUpButton.SetActive(false);
            powerUpList[0].SetActive(true);
            powerUpList[1].SetActive(false);
            powerUpList[2].SetActive(false);
            bulletsButton.SetActive(false);
            bulletsList[0].SetActive(true);
            bulletsList[1].SetActive(false);
            bulletsList[2].SetActive(false);
        }
        if (upgradesList.powerUPUpgrade)
        {
            powerUpButton.SetActive(false);
            powerUpList[0].SetActive(false);
            powerUpList[1].SetActive(false);
            powerUpList[2].SetActive(true);
            bulletsButton.SetActive(true);
            bulletsList[0].SetActive(false);
            bulletsList[1].SetActive(true);
            bulletsList[2].SetActive(false);
        }
        if (upgradesList.bulletsUpgrade)
        {
            bulletsButton.SetActive(false);
            bulletsList[0].SetActive(false);
            bulletsList[1].SetActive(false);
            bulletsList[2].SetActive(true);
        }
        /////////////////////////////////////
        if (upgradesList.boosterUpgrade)
        {
            boosterButton.SetActive(false);
            boosterList[0].SetActive(false);
            boosterList[1].SetActive(false);
            boosterList[2].SetActive(true);
            maskingButton.SetActive(true);
            maskingList[0].SetActive(false);
            maskingList[1].SetActive(true);
            maskingList[2].SetActive(false);
            bombButton.SetActive(false);
            bombList[0].SetActive(true);
            bombList[1].SetActive(false);
            bombList[2].SetActive(false);
        }
        else
        {
            boosterButton.SetActive(true);
            boosterList[0].SetActive(false);
            boosterList[1].SetActive(true);
            boosterList[2].SetActive(false);
            maskingButton.SetActive(false);
            maskingList[0].SetActive(true);
            maskingList[1].SetActive(false);
            maskingList[2].SetActive(false);
            bombButton.SetActive(false);
            bombList[0].SetActive(true);
            bombList[1].SetActive(false);
            bombList[2].SetActive(false);
        }
        if (upgradesList.maskingUpgrade)
        {
            maskingButton.SetActive(false);
            maskingList[0].SetActive(false);
            maskingList[1].SetActive(false);
            maskingList[2].SetActive(true);
            bombButton.SetActive(true);
            bombList[0].SetActive(false);
            bombList[1].SetActive(true);
            bombList[2].SetActive(false);
        }
        if (upgradesList.bombUpgrade)
        {
            bombButton.SetActive(false);
            bombList[0].SetActive(false);
            bombList[1].SetActive(false);
            bombList[2].SetActive(true);
        }
        
        pointsCounter.text = "2";
    }
    private void Update()
    {
        if(upgradePoints == 1)
        {
            pointsCounter.text = "1";
        }
        else if(upgradePoints == 0)
        {
            pointsCounter.text = "0";
        }
    }
    /////////////////////////////////////////////////
    public void ShieldUpgrade()
    {
        if(upgradePoints > 0)
        {
            upgradesList.shieldUpgrade = true;
            shieldButton.SetActive(false);
            shieldList[1].SetActive(false);
            shieldList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            savingShieldButton.SetActive(true);
            savingShieldList[0].SetActive(false);
            savingShieldList[1].SetActive(true);
        }
    }
    public void SavingShieldUpgrade()
    {
        if (upgradePoints > 0)
        {
            upgradesList.savingShieldUpgrade = true;
            savingShieldButton.SetActive(false);
            savingShieldList[1].SetActive(false);
            savingShieldList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            extraToughButton.SetActive(true);
            extraToughList[0].SetActive(false);
            extraToughList[1].SetActive(true);
        }
    }
    public void ExtraToughUpgrade()
    {
        if (upgradePoints > 0)
        {
            playerCurrentStats.playerMaxHP++;
            upgradesList.extraToughUpgrade = true;
            extraToughButton.SetActive(false);
            extraToughList[1].SetActive(false);
            extraToughList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
        }
    }
    //////////////////////////////////////////////////
    public void FasterShootingUpgrade()
    {
        if (upgradePoints > 0)
        {
            playerCurrentStats.shootingSpeed -= upgradesList.fasterShootSpeed;
            upgradesList.fasterShootUpgrade = true;
            fasterShootButton.SetActive(false);
            fasterShootList[1].SetActive(false);
            fasterShootList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            powerUpButton.SetActive(true);
            powerUpList[0].SetActive(false);
            powerUpList[1].SetActive(true);
        }
    }
    public void PowerUpUpgrade()
    {
        if(upgradePoints > 0)
        {
            playerCurrentStats.damage++;
            upgradesList.powerUPUpgrade = true;
            powerUpButton.SetActive(false);
            powerUpList[1].SetActive(false);
            powerUpList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            bulletsButton.SetActive(true);
            bulletsList[0].SetActive(false);
            bulletsList[1].SetActive(true);
        }
    }
    public void BulletsUpgrade()
    {
        if (upgradePoints > 0)
        {
            upgradesList.bulletsUpgrade = true;
            bulletsButton.SetActive(false);
            bulletsList[1].SetActive(false);
            bulletsList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
        }
    }
    /////////////////////////////////////////////
    public void BoosterUpgrade()
    {
        if(upgradePoints > 0)
        {
            upgradesList.boosterUpgrade = true;
            boosterButton.SetActive(false);
            boosterList[1].SetActive(false);
            boosterList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            maskingButton.SetActive(true);
            maskingList[0].SetActive(false);
            maskingList[1].SetActive(true);
        }
    }
    public void MaskingUpgrade()
    {
        if (upgradePoints > 0)
        {
            upgradesList.maskingUpgrade = true;
            maskingButton.SetActive(false);
            maskingList[1].SetActive(false);
            maskingList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
            bombButton.SetActive(true);
            bombList[0].SetActive(false);
            bombList[1].SetActive(true);
        }
    }
    public void BombUpgrade()
    {
        if (upgradePoints > 0)
        {
            upgradesList.bombUpgrade = true;
            bombButton.SetActive(false);
            bombList[1].SetActive(false);
            bombList[2].SetActive(true);
            AudioManager.Instance.PlaySound("buttonUpgrades");
            upgradePoints--;
        }
    }
    /////////////////////////////////////////
    public void NextLevel()
    {
        if(upgradePoints == 0)
        {
            AudioManager.Instance.PlaySound("button");
            SceneManager.LoadScene(nextLevelName);
        }
    }
}
