using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIExitNoButtons : MonoBehaviour
{
    [SerializeField] private GameObject ui;
    [SerializeField] private GameObject buttons;
    public string sound;

    public void Exit()
    {
        AudioManager.Instance.PlaySound(sound);
        buttons.SetActive(true);
        ui.SetActive(false);
    }
}
