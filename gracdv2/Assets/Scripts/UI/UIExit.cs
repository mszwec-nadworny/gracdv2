using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIExit : MonoBehaviour
{
    [SerializeField] private GameObject ui;
    public string sound;

    public void Exit()
    {
        AudioManager.Instance.PlaySound(sound);
        ui.SetActive(false);
    }
}
