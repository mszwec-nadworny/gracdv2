using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public static InGameUI Instance;

    [SerializeField] private GameObject hp1;
    [SerializeField] private GameObject hp2;
    [SerializeField] private GameObject hp3;
    [SerializeField] private GameObject hp4;
    [SerializeField] private GameObject hpBonus;
    [SerializeField] private GameObject overlay1;
    [SerializeField] private GameObject overlay2;
    [SerializeField] private GameObject overlay3;

    public Image bar;
    public Animator anim;
    public float thermAnimTime = 1f;

    public GameObject debuffIcon;
    public GameObject debuffShootIcon;

    public GameObject speedBoostIcon;
    public Image speedBoostFill;
    private float speedBoostTime;
    private float speedBoostFillAmount;
    private float speedBoostMaxTime;

    public GameObject bulletSpeedIcon;
    public Image bulletSpeedFill;
    private float bulletSpeedTime;
    private float bulletSpeedFillAmount;
    private float bulletSpeedMaxTime;

    public GameObject damageIcon;
    public Image damageFill;
    private float damageTime;
    private float damageFillAmount;
    private float damageMaxTime;

    public GameObject tripleShotIcon;
    public Image tripleShotFill;
    private float tripleShotTime;
    private float tripleShotFillAmount;
    private float tripleShotMaxTime;

    [SerializeField] private UpgradesSO upgradesList;
    public GameObject shieldBuff;
    //public GameObject shieldColor;
    public Image shieldFill;
    private float shieldTime;
    private float shieldFillAmount;
    ///
    public GameObject mask;
    public GameObject maskColor;
    public Image maskFill;
    private float maskTime;
    private float maskFillAmount;
    ///
    public GameObject bomb;
    public GameObject bombColor;
    public Image bombFill;
    private float bombTime;
    private float bombFillAmount;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void Start()
    {
        hp1.SetActive(true);
        hp2.SetActive(true);
        hp3.SetActive(true);
        hp4.SetActive(true);
        if (!upgradesList.extraToughUpgrade)
        {
            hpBonus.SetActive(false);
        }
        else
        {
            hpBonus.SetActive(true);
        }
        if (!upgradesList.savingShieldUpgrade)
        {
            shieldBuff.SetActive(false);
        }
        else
        {
            shieldBuff.SetActive(true);
        }
        if (!upgradesList.maskingUpgrade)
        {
            mask.SetActive(false);
        }
        else
        {
            mask.SetActive(true);
        }
        if (!upgradesList.bombUpgrade)
        {
            bomb.SetActive(false);
        }
        else
        {
            bomb.SetActive(true);
        }
        debuffIcon.SetActive(false);
        debuffShootIcon.SetActive(false);
        speedBoostIcon.SetActive(false);
        bulletSpeedIcon.SetActive(false);
        damageIcon.SetActive(false);
        tripleShotIcon.SetActive(false);
        overlay1.SetActive(false);
        overlay2.SetActive(false);
        overlay3.SetActive(false);
        anim.SetBool("isGrowing", false);
        StopAllCoroutines();
        GetCurrentFill();
    }

    private void Update()
    {
        GetCurrentFill();
        HPDisplay();
        if(PlayerManager.Instance.shieldCooldown && Time.timeScale == 1f)
        {
            shieldTime += Time.deltaTime;
            shieldFillAmount = shieldTime / upgradesList.shieldCooldown;
            //Debug.Log(shieldFillAmount);
            shieldFill.fillAmount = shieldFillAmount;
        }
        if (PlayerManager.Instance.maskCooldown && Time.timeScale == 1f)
        {
            maskTime += Time.deltaTime;
            maskFillAmount = maskTime / upgradesList.maskingCooldown;
            //Debug.Log(maskFillAmount);
            maskFill.fillAmount = maskFillAmount;
        }
        if (PlayerManager.Instance.bombCooldown && Time.timeScale == 1f)
        {
            bombTime += Time.deltaTime;
            bombFillAmount = bombTime / upgradesList.bombCooldown;
            //Debug.Log(bombFillAmount);
            bombFill.fillAmount = bombFillAmount;
        }
        if(PlayerManager.Instance.speedBuffCounter > 0)
        {
            speedBoostTime += Time.deltaTime;
            speedBoostFillAmount = 1 - (speedBoostTime / speedBoostMaxTime);
            speedBoostFill.fillAmount = speedBoostFillAmount;
        }
        if(PlayerManager.Instance.shootingSpeedBoostCounter > 0)
        {
            bulletSpeedTime += Time.deltaTime;
            bulletSpeedFillAmount = 1 - (bulletSpeedTime / bulletSpeedMaxTime);
            bulletSpeedFill.fillAmount = bulletSpeedFillAmount;
        }
        if(PlayerManager.Instance.damageBoostCounter > 0)
        {
            damageTime += Time.deltaTime;
            damageFillAmount = 1 - (damageTime / damageMaxTime);
            damageFill.fillAmount = damageFillAmount;
        }
        if(PlayerManager.Instance.tripleShotBoostCounter > 0)
        {
            tripleShotTime += Time.deltaTime;
            tripleShotFillAmount = 1 - (tripleShotTime / tripleShotMaxTime);
            tripleShotFill.fillAmount = tripleShotFillAmount;
        }
    }

    public void HPDisplay()
    {
        switch (PlayerManager.Instance.playerCurrentHP)
        {

            case 4:
                hp1.SetActive(true);
                hp2.SetActive(true);
                hp3.SetActive(true);
                hp4.SetActive(true);
                overlay1.SetActive(false);
                overlay2.SetActive(false);
                overlay3.SetActive(false);
                break;
            case 3:
                hp1.SetActive(true);
                hp2.SetActive(true);
                hp3.SetActive(true);
                hp4.SetActive(false);
                overlay1.SetActive(false);
                overlay2.SetActive(false);
                overlay3.SetActive(false);
                break;
            case 2:
                hp1.SetActive(true);
                hp2.SetActive(true);
                hp3.SetActive(false);
                hp4.SetActive(false);
                overlay1.SetActive(true);
                overlay2.SetActive(false);
                overlay3.SetActive(false);
                break;
            case 1:
                hp1.SetActive(true);
                hp2.SetActive(false);
                hp3.SetActive(false);
                hp4.SetActive(false);
                overlay1.SetActive(false);
                overlay2.SetActive(true);
                overlay3.SetActive(false);
                break;
            default:
                hp1.SetActive(false);
                hp2.SetActive(false);
                hp3.SetActive(false);
                hp4.SetActive(false);
                overlay1.SetActive(false);
                overlay2.SetActive(false);
                overlay3.SetActive(true);
                break;
        }
    }

    private void GetCurrentFill()
    {
        float fill = SicknessManager.Instance.currentSicknessLvl / SicknessManager.Instance.maxSicknessLvl;
        bar.fillAmount = fill;
    }

    public void ThermometrPuls()
    {
        StartCoroutine(Puls());
    }

    IEnumerator Puls()
    {
        anim.SetBool("isGrowing", true);
        yield return new WaitForSeconds(thermAnimTime);
        anim.SetBool("isGrowing", false);
    }
    public void ShieldUpgradeUI()
    {
        //shieldColor.SetActive(false);
        shieldTime = 0;
        shieldFillAmount = 0;
    }

    public void MaskUpgradeUI()
    {
        maskColor.SetActive(false);
        maskTime = 0;
        maskFillAmount = 0;
    }

    public void BombUpgradeUI()
    {
        bombColor.SetActive(false);
        bombTime = 0;
        bombFillAmount = 0;
    }

    public void SpeedBoostUI(float time)
    {
        speedBoostTime = 0;
        speedBoostFillAmount = 0;
        speedBoostMaxTime = time;
    }

    public void BulletSpeedUI(float time)
    {
        bulletSpeedTime = 0;
        bulletSpeedFillAmount = 0;
        bulletSpeedMaxTime = time;
    }

    public void DamageUI(float time)
    {
        damageTime = 0;
        damageFillAmount = 0;
        damageMaxTime = time;
    }

    public void TripleShotUI(float time)
    {
        tripleShotTime = 0;
        tripleShotFillAmount = 0;
        tripleShotMaxTime = time;
    }
}
