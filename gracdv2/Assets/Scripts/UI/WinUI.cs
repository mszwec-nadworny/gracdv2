using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Unity.VisualScripting;

public class WinUI : MonoBehaviour
{
    public static WinUI Instance;

    [SerializeField] private StatisticsGO gameStats;

    [SerializeField] private TextMeshProUGUI timeTXT;

    [SerializeField] private TextMeshProUGUI lostHPTXT;

    [SerializeField] private TextMeshProUGUI sicknessTXT;

    [SerializeField] private TextMeshProUGUI killedEnemiesTXT;

    [SerializeField] private TextMeshProUGUI rankTXT;
    [SerializeField] private GameObject rankS;
    [SerializeField] private GameObject rankA;
    [SerializeField] private GameObject rankB;
    [SerializeField] private GameObject rankC;
    [SerializeField] private GameObject rankD;
    [SerializeField] private float pointsMinS;
    [SerializeField] private float pointsMinA;
    [SerializeField] private float pointsMinB;
    [SerializeField] private float pointsMinC;
    [SerializeField] private bool lastLevel = false;
    [SerializeField] private float pointsPerSecond;
    //[SerializeField] private float pointsPerEnemy;
    [SerializeField] private float pointsPerStation;
    [SerializeField] private float pointsPerLostHP;
    [SerializeField] private float bossGoodTime;
    [SerializeField] private float bossPointsPerSecond;
    private float points;
    private float gameTime;
    private float bossTime;
    private int killedEnemies;
    private int infectedStations;
    private int damageReceived;

    [SerializeField] private UIManager uim;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        SicknessManager.Instance.restartStats.time = 0;
        SicknessManager.Instance.restartStats.enemiesKilled = 0;
        SicknessManager.Instance.restartStats.damageReceived = 0;
        SicknessManager.Instance.restartStats.stationsInfected = 0;
        SicknessManager.Instance.restartStats.level1Points = 0;
    }
    public void DisplayAll()
    {
        DisplayTime();
        DisplayDamage();
        DisplaySickness();
        DisplayEnemies();
        DisplayRank();
    }
    private void DisplayTime()
    {
        float timer = TimeManager.Instance.currentTime;
        float minutes = Mathf.FloorToInt(timer / 60);
        float seconds = Mathf.FloorToInt(timer % 60);
        timeTXT.text = "Time: " + string.Format("{0:00}:{1:00}", minutes, seconds);
        gameTime = TimeManager.Instance.preBossTime;
        bossTime = TimeManager.Instance.currentTime - gameTime;
    }
    private void DisplaySickness()
    {
        sicknessTXT.text = $"Infected stations: {SicknessManager.Instance.infectedStations}";
        infectedStations = SicknessManager.Instance.infectedStations;
    }
    private void DisplayEnemies()
    {        
        killedEnemies = SicknessManager.Instance.killedEnemies;
        killedEnemiesTXT.text = $"Killed Enemies: {killedEnemies}";
    }
    private void DisplayDamage()
    {
        damageReceived = SicknessManager.Instance.damageReceived;
        lostHPTXT.text = $"Health Lost: {damageReceived}";
    }
    public void GoToMainMenu()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(0);
    }

    public void UpgradeUI()
    {
        AudioManager.Instance.PlaySound("button");
        if (!lastLevel)
        {
            uim.upgradesON = true;
        }
        else
        {
            uim.endGameON = true;
        }
    }
    private void DisplayRank()
    {
        PointsCalculate();
        RankDisplay();
        SaveStats();
    }
    private void PointsCalculate()
    {
        points = PlayerManager.Instance.startingPoints;
        points -= gameTime * pointsPerSecond;
        //points += killedEnemies * pointsPerEnemy;
        points += infectedStations * pointsPerStation;
        points -= damageReceived * pointsPerLostHP;
        points += (bossGoodTime - bossTime) * bossPointsPerSecond;
        //Debug.Log(points);
        if (points > 100) points = 100;
        //Debug.Log(points);
    }
    private void RankDisplay()
    {
        if (points >= pointsMinS)
        {
            // rank S
            rankS.SetActive(true);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinA)
        {
            // rank A
            rankS.SetActive(false);
            rankA.SetActive(true);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinB)
        {
            // rank B
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(true);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinC)
        {
            // rank C
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(true);
            rankD.SetActive(false);
        }
        else
        {
            // rank D
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(true);
        }
    }
    private void SaveStats()
    {
        int index = SceneManager.GetActiveScene().buildIndex;
        switch (index)
        {
            case 1:
                gameStats.level1Points = points;
                break;
            case 2:
                gameStats.level2Points = points;
                break;
            case 3:
                gameStats.level3Points = points;
                break;
            case 4:
                gameStats.level4Points = points;
                break;
            default:
                break;
        }
        gameStats.time += TimeManager.Instance.currentTime;
        gameStats.enemiesKilled += killedEnemies;
        gameStats.damageReceived += damageReceived;
        gameStats.stationsInfected += infectedStations;
    }
}
