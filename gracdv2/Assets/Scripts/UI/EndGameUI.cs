using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameUI : MonoBehaviour
{
    public static EndGameUI Instance;

    [SerializeField] private StatisticsGO gameStats;

    [SerializeField] private TextMeshProUGUI timeTXT;
    [SerializeField] private TextMeshProUGUI killedEnemiesTXT;
    [SerializeField] private TextMeshProUGUI pointsTXT;

    [SerializeField] private GameObject rankS;
    [SerializeField] private GameObject rankA;
    [SerializeField] private GameObject rankB;
    [SerializeField] private GameObject rankC;
    [SerializeField] private GameObject rankD;
    [SerializeField] private float pointsMinS;
    [SerializeField] private float pointsMinA;
    [SerializeField] private float pointsMinB;
    [SerializeField] private float pointsMinC;

    private float points;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        //DisplayAll();
    }
    public void DisplayAll()
    {
        DisplayTime();
        DisplayPoints();
        DisplayEnemies();
        DisplayRank();
    }
    private void DisplayTime()
    {
        float timer = gameStats.time;
        float minutes = Mathf.FloorToInt(timer / 60);
        float seconds = Mathf.FloorToInt(timer % 60);
        timeTXT.text = "Total Time: " + string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    private void DisplayEnemies()
    {
        killedEnemiesTXT.text = $"Killed Enemies: {gameStats.enemiesKilled}";
    }
    private void DisplayPoints()
    {
        points = (gameStats.level1Points + gameStats.level2Points + gameStats.level3Points + gameStats.level4Points) / 4;
        points = Mathf.Round(points * 100.0f) * 0.01f;
        pointsTXT.text = $"Final Points: {points}";
    }
    private void DisplayRank()
    {
        if (points >= pointsMinS)
        {
            // rank S
            rankS.SetActive(true);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinA)
        {
            // rank A
            rankS.SetActive(false);
            rankA.SetActive(true);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinB)
        {
            // rank B
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(true);
            rankC.SetActive(false);
            rankD.SetActive(false);
        }
        else if (points >= pointsMinC)
        {
            // rank C
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(true);
            rankD.SetActive(false);
        }
        else
        {
            // rank D
            rankS.SetActive(false);
            rankA.SetActive(false);
            rankB.SetActive(false);
            rankC.SetActive(false);
            rankD.SetActive(true);
        }
    }
    public void GoToMainMenu()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        AudioManager.Instance.PlaySound("button");
        Application.Quit();
    }
}
