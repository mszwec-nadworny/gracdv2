using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    //public float timeMax;
    [HideInInspector] public float currentTime;
    [HideInInspector] public float preBossTime;
    public TextMeshProUGUI timerTxt;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        StopAllCoroutines();
        currentTime = 0;
    }
    void Start()
    {
        StartCoroutine(Timing());
    }

    IEnumerator Timing()
    {
        do
        {
            Display();        
            yield return new WaitForSeconds(1f);
            currentTime++;
        } while (!UIManager.Instance.isOver);
    }
    private void Display()
    {
        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);
        timerTxt.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
