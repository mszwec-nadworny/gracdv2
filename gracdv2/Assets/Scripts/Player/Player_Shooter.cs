using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class Player_Shooter : MonoBehaviour
{
    public static Player_Shooter Instance;

    public Transform bulletSpawn;
    public Transform bulletSpawn2;
    public Transform bulletSpawn3;
    public GameObject bulletPrefab;
    public GameObject bulletPrefabExplode;
    private bool canUShoot;

    [HideInInspector] public bool isTripleShot;

    [HideInInspector] public bool isDebuffed;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
        canUShoot = true;
        isTripleShot = false;
        StopAllCoroutines();
        isDebuffed= false;
    }
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse0)) && canUShoot && !UIManager.Instance.isPaused)
        {
            AudioManager.Instance.PlaySound("bulletPlayer");
            Shoot();
            if (isTripleShot) Shoot3();
            StartCoroutine(CantShoot());
        }
    }

    private void Shoot()
    {
        if (PlayerManager.Instance.upgradesList.bulletsUpgrade)
        {
            GameObject cb = Instantiate(bulletPrefabExplode, bulletSpawn.position, bulletPrefab.transform.rotation);
            Rigidbody rig = cb.GetComponent<Rigidbody>();

            rig.AddForce(bulletSpawn.right * PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);
        }
        else
        {
            GameObject cb = Instantiate(bulletPrefab, bulletSpawn.position, bulletPrefab.transform.rotation);
            Rigidbody rig = cb.GetComponent<Rigidbody>();

            rig.AddForce(bulletSpawn.right*PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);
        }
    }

    private void Shoot3()
    {
        if (PlayerManager.Instance.upgradesList.bulletsUpgrade)
        {
            GameObject cb2 = Instantiate(bulletPrefabExplode, bulletSpawn2.position, bulletPrefab.transform.rotation);
            Rigidbody rig2 = cb2.GetComponent<Rigidbody>();
            rig2.AddForce(bulletSpawn2.right * PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);

            GameObject cb3 = Instantiate(bulletPrefabExplode, bulletSpawn3.position, bulletPrefab.transform.rotation);
            Rigidbody rig3 = cb3.GetComponent<Rigidbody>();
            rig3.AddForce(bulletSpawn3.right * PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);
        }
        else
        {
            GameObject cb2 = Instantiate(bulletPrefab, bulletSpawn2.position, bulletPrefab.transform.rotation);
            Rigidbody rig2 = cb2.GetComponent<Rigidbody>();
            rig2.AddForce(bulletSpawn2.right * PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);

            GameObject cb3 = Instantiate(bulletPrefab, bulletSpawn3.position, bulletPrefab.transform.rotation);
            Rigidbody rig3 = cb3.GetComponent<Rigidbody>();
            rig3.AddForce(bulletSpawn3.right * PlayerManager.Instance.playerStats.bulletSpeed, ForceMode.Impulse);
        }
    }

    public void TripleShotActivate(float time)
    {
        float newTime = time;
        if (PlayerManager.Instance.upgradesList.boosterUpgrade) newTime += PlayerManager.Instance.upgradesList.boosterAdditionalTime;
        InGameUI.Instance.tripleShotIcon.SetActive(true);
        StartCoroutine(TripleShootActive(newTime));
    }

    public void ShootingSpeedBuff(float shootingSpeed, float time)
    {
        float newTime = time;
        if (PlayerManager.Instance.upgradesList.boosterUpgrade) newTime += PlayerManager.Instance.upgradesList.boosterAdditionalTime;
        InGameUI.Instance.bulletSpeedIcon.SetActive(true);
        StartCoroutine(ShootingSpeedChange(shootingSpeed, newTime));
    }

    public void ShootingSpeedDebuff(float shootingSpeed, float time)
    {
        InGameUI.Instance.debuffShootIcon.SetActive(true);
        isDebuffed= true;
        StartCoroutine(ShootingSpeedDebuffed(shootingSpeed, time));
    }

    IEnumerator CantShoot()
    {
        canUShoot= false;
        yield return new WaitForSeconds(PlayerManager.Instance.playerCurrentShootingSpeed);
        canUShoot= true;
    }

    IEnumerator TripleShootActive(float time)
    {
        isTripleShot = true;
        InGameUI.Instance.TripleShotUI(time);
        yield return new WaitForSeconds(time);
        if(PlayerManager.Instance.tripleShotBoostCounter <= 1)
        {
            isTripleShot = false;
            InGameUI.Instance.tripleShotIcon.SetActive(false);
        }
        PlayerManager.Instance.tripleShotBoostCounter--;
    }

    IEnumerator ShootingSpeedChange(float newShootingSpeed, float time)
    {
        if(PlayerManager.Instance.shootingSpeedBoostCounter <= 1)
        {
            PlayerManager.Instance.playerCurrentShootingSpeed = PlayerManager.Instance.playerCurrentShootingSpeed - newShootingSpeed;
        }
        InGameUI.Instance.BulletSpeedUI(time);
        yield return new WaitForSeconds(time);
        if(PlayerManager.Instance.shootingSpeedBoostCounter <= 1)
        {        
            PlayerManager.Instance.playerCurrentShootingSpeed = PlayerManager.Instance.playerCurrentShootingSpeed + newShootingSpeed;
            InGameUI.Instance.bulletSpeedIcon.SetActive(false);
        }
        PlayerManager.Instance.shootingSpeedBoostCounter--;
    }

    IEnumerator ShootingSpeedDebuffed(float newShootingSpeed, float time)
    {
        PlayerManager.Instance.playerCurrentShootingSpeed = PlayerManager.Instance.playerCurrentShootingSpeed + newShootingSpeed;
        yield return new WaitForSeconds(time);
        PlayerManager.Instance.playerCurrentShootingSpeed = PlayerManager.Instance.playerCurrentShootingSpeed - newShootingSpeed;
        isDebuffed = false;
        InGameUI.Instance.debuffShootIcon.SetActive(false);
    }
}
