using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Player", menuName = "ScriptableObject/PlayerScriptableObject")]
public class PlayerSO : ScriptableObject
{
    public int playerMaxHP;
    public float speed;
    public float pushSpeed;
    public int damage;
    public float shootingSpeed;
    public float bulletSpeed;
}
