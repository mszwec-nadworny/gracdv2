using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShield : MonoBehaviour
{
    public static PlayerShield Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet_Enemy") && !PlayerManager.Instance.isImmune && !PlayerManager.Instance.maskOn)
        {
            Destroy(other.gameObject);
            StartDestroyShield();
        }
        else if(other.CompareTag("Bullet_Enemy"))
        {
            Destroy(other.gameObject);
        }
    }

    public void ShieldON()
    {
        PlayerManager.Instance.isShieldON = true;
    }

    public void DestroyShield()
    {
        PlayerManager.Instance.DestroyShield();
    }

    public void StartDestroyShield()
    {
        StartCoroutine(WaitToDestroyShield());
    }

    IEnumerator WaitToDestroyShield()
    {
        yield return new WaitForEndOfFrame();
        DestroyShield();
    }
}
