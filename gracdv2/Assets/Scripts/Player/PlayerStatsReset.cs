using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsReset : MonoBehaviour
{
    public PlayerSO startingStats;
    public PlayerSO playerCurrentStats;
    public UpgradesSO upgrades;
    public StatisticsGO gameStats;
    public StatisticsGO restartStats;
    private void Awake()
    {
        playerCurrentStats.playerMaxHP = startingStats.playerMaxHP;
        playerCurrentStats.speed = startingStats.speed;
        playerCurrentStats.pushSpeed = startingStats.pushSpeed;
        playerCurrentStats.damage = startingStats.damage;
        playerCurrentStats.shootingSpeed = startingStats.shootingSpeed;
        playerCurrentStats.bulletSpeed = startingStats.bulletSpeed;

        //upgrades reset
        upgrades.shieldUpgrade = false;
        upgrades.savingShieldUpgrade= false;
        upgrades.extraToughUpgrade= false;
        upgrades.fasterShootUpgrade= false;
        upgrades.powerUPUpgrade = false;   
        upgrades.bulletsUpgrade= false;
        upgrades.boosterUpgrade = false;
        upgrades.maskingUpgrade= false;
        upgrades.bombUpgrade= false;

        // game stats reset
        gameStats.level1Points = 0;
        gameStats.level2Points = 0;
        gameStats.level3Points = 0;
        gameStats.level4Points = 0;
        gameStats.time = 0;
        gameStats.enemiesKilled = 0;
        gameStats.damageReceived = 0;
        gameStats.stationsInfected = 0;

        // game restart stats reset
        restartStats.level1Points = 0;
        restartStats.level2Points = 0;
        restartStats.level3Points = 0;
        restartStats.level4Points = 0;
        restartStats.time = 0;
        restartStats.enemiesKilled = 0;
        restartStats.damageReceived = 0;
        restartStats.stationsInfected = 0;
    }
}
