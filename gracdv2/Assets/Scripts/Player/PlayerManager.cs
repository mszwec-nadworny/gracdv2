using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    private Rigidbody rb;
    public PlayerSO playerStats;

    public int playerCurrentHP;
    public float playerCurrentSpeed;
    public int playerCurrentDmg;
    public float playerCurrentShootingSpeed;

    [HideInInspector] public bool takeNoDmg;
    public float noDmgTime;

    private bool isMoving;
    [HideInInspector] public bool isDebuffed;
    [HideInInspector] public bool isInWind;
    [HideInInspector] public int speedBuffCounter;
    [HideInInspector] public int damageBoostCounter;
    [HideInInspector] public int shootingSpeedBoostCounter;
    [HideInInspector] public int tripleShotBoostCounter;
    [HideInInspector] public bool isShieldON;
    public GameObject playerShield;

    // UPGRADES
    public UpgradesSO upgradesList;
    [HideInInspector] public bool shieldCooldown;
    [HideInInspector] public bool maskOn;
    [HideInInspector] public bool maskCooldown;
    //public GameObject mask;
    public ParticleSystem psMask;

    [HideInInspector] public bool isImmune;
    [HideInInspector] public bool bombCooldown;
    [HideInInspector] public int bombDMG;
    public GameObject immunity;
    public ParticleSystem psBomb;
    public float startingPoints;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }else if(Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }

    private void Start()
    {
        StopAllCoroutines();
        rb= GetComponent<Rigidbody>();
        playerCurrentHP = playerStats.playerMaxHP;
        playerCurrentSpeed = playerStats.speed;
        playerCurrentDmg = playerStats.damage;
        playerCurrentShootingSpeed = playerStats.shootingSpeed;
        takeNoDmg = false;
        isMoving= false;
        isDebuffed = false;
        isInWind = false;
        speedBuffCounter = 0;
        damageBoostCounter = 0;
        shootingSpeedBoostCounter = 0;
        tripleShotBoostCounter = 0;
        isShieldON = false;
        playerShield.SetActive(false);
        //mask.SetActive(false);
        immunity.SetActive(false);
        if (upgradesList.shieldUpgrade) ShieldON();
        shieldCooldown = false;
        maskOn = false;
        maskCooldown = false;
        isImmune = false;
        bombCooldown = false;
        bombDMG = 0;
    }
    void Update()
    {
        if (!isMoving)
            transform.Translate(Vector3.left * Time.deltaTime * playerStats.pushSpeed, Space.World);
        if (playerCurrentHP == 1 && upgradesList.savingShieldUpgrade && !shieldCooldown)
        {
            ShieldON();
            StartCoroutine(ShieldCooldown(upgradesList.shieldCooldown));
        }
        if(Input.GetKeyDown(KeyCode.Q) && upgradesList.maskingUpgrade && !maskCooldown)
        {
            StartCoroutine(MaskOn(upgradesList.maskingTime));
        }
        if(Input.GetKeyDown(KeyCode.E) && upgradesList.bombUpgrade && !bombCooldown)
        {
            bombDMG = 0;
            StartCoroutine(BombImmunity(upgradesList.bombTime));
        }
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal") * playerCurrentSpeed;
        float vertical = Input.GetAxis("Vertical")* playerCurrentSpeed;
        isMoving = true;
        //rb.AddForce(new Vector3(horizontal, vertical, 0), ForceMode.VelocityChange);
        rb.velocity = new Vector3(horizontal, vertical, 0);
        isMoving= false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet_Enemy") && isImmune)
        {
            bombDMG++;
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Bullet_Enemy") && !isShieldON && !maskOn)
        {
            if (!takeNoDmg)
            {
                AudioManager.Instance.PlaySound("damagePlayer");
                playerCurrentHP--;
                SicknessManager.Instance.damageReceived++;
                TakeNoDmgFunction();
            }
            Destroy(other.gameObject);
        }
        else if(other.CompareTag("Bullet_Enemy"))
        {
            Destroy(other.gameObject);
        }
    }

    public void ShieldON()
    {
        playerShield.SetActive(true);
        isShieldON= true;
    }
    public void DestroyShield()
    {
        TakeNoDmgFunction();
        isShieldON = false;
        playerShield.SetActive(false);
    }

    public void ChangeSpeed(float speedDebuff, float time)
    {

        StartCoroutine(SpeedDebuffChange(speedDebuff, time));
    }

    public void SpeedBuff(float speedBuff, float time)
    {
        float newTime = time;
        if (upgradesList.boosterUpgrade) newTime += upgradesList.boosterAdditionalTime;
        StartCoroutine(SpeedBuffChange(speedBuff, newTime));
    }

    public void DmgBuff(int dmgBonus, float time)
    {
        float newTime = time;
        if (upgradesList.boosterUpgrade) newTime += upgradesList.boosterAdditionalTime;
        int newDmg = playerCurrentDmg + dmgBonus;
        InGameUI.Instance.damageIcon.SetActive(true);
        StartCoroutine(DmgChange(newDmg, newTime));
    }

    public void TakeNoDmgFunction()
    {
        StartCoroutine(TakeNoDmg(noDmgTime));
    }

    IEnumerator SpeedDebuffChange(float speedDebuff, float time)
    {
        isDebuffed = true;
        playerCurrentSpeed -= speedDebuff;
        InGameUI.Instance.debuffIcon.SetActive(true);
        yield return new WaitForSeconds(time);
        playerCurrentSpeed += speedDebuff;
        if(!isInWind)
        {
            InGameUI.Instance.debuffIcon.SetActive(false);
        }
        isDebuffed = false;
    }

    IEnumerator SpeedBuffChange(float speedBuff, float time)
    {
        playerCurrentSpeed += speedBuff;
        speedBuffCounter++;
        InGameUI.Instance.speedBoostIcon.SetActive(true);
        InGameUI.Instance.SpeedBoostUI(time);
        yield return new WaitForSeconds(time);
        playerCurrentSpeed -= speedBuff;
        if (speedBuffCounter <= 1)  //playerCurrentSpeed <= playerStats.speed)
        {
            InGameUI.Instance.speedBoostIcon.SetActive(false);
            //Debug.Log("IconOff");
        }
        //Debug.Log("BuffOff");
        speedBuffCounter--;
    }

    IEnumerator DmgChange(int newDmg, float time)
    {
        playerCurrentDmg = newDmg;
        InGameUI.Instance.DamageUI(time);
        yield return new WaitForSeconds(time);
        if(damageBoostCounter <= 1)
        {
            playerCurrentDmg = playerStats.damage;
            InGameUI.Instance.damageIcon.SetActive(false);
        }
        damageBoostCounter--;
    }

    IEnumerator TakeNoDmg(float time)
    {
        takeNoDmg = true;
        immunity.SetActive(takeNoDmg);
        yield return new WaitForSeconds(time);
        takeNoDmg = false;
        immunity.SetActive(takeNoDmg);
    }

    IEnumerator ShieldCooldown(float time)
    {
        shieldCooldown = true;
        InGameUI.Instance.ShieldUpgradeUI();
        yield return new WaitForSeconds(time);        
        //InGameUI.Instance.shieldColor.SetActive(true);
        shieldCooldown = false;
    }
    IEnumerator MaskOn(float time)
    {
        maskOn = true;
        //mask.SetActive(maskOn);
        psMask.Play();
        InGameUI.Instance.maskColor.SetActive(false);
        yield return new WaitForSeconds(time);
        maskOn = false;
        psMask.Stop();
        //mask.SetActive(maskOn);
        StartCoroutine(MaskCoolDown(upgradesList.maskingCooldown));
    }
    IEnumerator MaskCoolDown(float time)
    {
        maskCooldown = true;
        InGameUI.Instance.MaskUpgradeUI();
        yield return new WaitForSeconds(time);
        InGameUI.Instance.maskColor.SetActive(true);
        maskCooldown = false;
    }

    private void BombExplosion()
    {
        Vector3 startPosition = Vector3.zero;
        RaycastHit[] hits;
        hits = Physics.SphereCastAll(startPosition, upgradesList.bombRadius, transform.forward, 0);

        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                if (hit.collider.GetComponent<EnemyHpManager>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManager>().currentHP -= bombDMG;
                }
                else if (hit.collider.GetComponent<EnemyHpManagerNotShooting>() != null)
                {
                    hit.collider.GetComponent<EnemyHpManagerNotShooting>().currentHP -= bombDMG;
                }
                else if (hit.collider.GetComponent<ExplodingEnemy>() != null)
                {
                    hit.collider.GetComponent<ExplodingEnemy>().currentHP -= bombDMG;
                }
            }
        }
    }
    IEnumerator BombImmunity(float time)
    {
        isImmune = true;
        immunity.SetActive(isImmune);
        InGameUI.Instance.bombColor.SetActive(false);
        //Debug.Log("immunity");
        yield return new WaitForSeconds(time);
        isImmune = false;
        immunity.SetActive(isImmune);
        psBomb.Play();
        //Debug.Log(bombDMG);
        BombExplosion();
        //Debug.Log("explode");
        StartCoroutine(BombCooldown(time));
    }

    IEnumerator BombCooldown(float time)
    {
        bombCooldown = true;
        InGameUI.Instance.BombUpgradeUI();
        yield return new WaitForSeconds(time);
        InGameUI.Instance.bombColor.SetActive(true);
        bombCooldown = false;
    }
}
