using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stats", menuName = "ScriptableObject/Statistics")]
public class StatisticsGO: ScriptableObject
{
    public float level1Points;
    public float level2Points;
    public float level3Points;
    public float level4Points;
    public float time;
    public int enemiesKilled;
    public int damageReceived;
    public int stationsInfected;
}
