using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealerManager : MonoBehaviour
{
    public static HealerManager Instance;

    public List<Transform> healerSpawnPoints;
    [SerializeField] private GameObject healerPrefab;
    public float spawnTime;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(Instance.gameObject);
            Instance = this;
        }
    }
    public void SpawnHealer()
    {
        Instantiate(healerPrefab, healerSpawnPoints[Random.Range(0, healerSpawnPoints.Count)].position, Quaternion.identity);
    }
    public void WaitToSpawnHealer()
    {
        StartCoroutine(WaitToSpawn());
        //Debug.Log("spawn");
    }
    IEnumerator WaitToSpawn()
    {
        yield return new WaitForSeconds(spawnTime);
        SpawnHealer();
    }
}
